export default {
  cacheReports: false,
  scanner: {
    throttle: false,
  },
  ci: {
    budget: {
      performance: 50,
      accessibility: 100,
      'best-practices': 90,
      seo: 90,
    },
  },
  debug: true,
}
