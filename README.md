## Personal Website: www.ornous.com

My personal blog, about me and contact

## Motivation

A place for people to learn about me and reach out.
A platform for me to practice my writing and web development skills

## Code style

This project uses prettier and eslint to maintain the standard js code style -

## Tech/framework used

<b>Built with</b>

- [Typescript](https://www.typescriptlang.org/)
- [React](https://www.reactjs.org/)
- [NextJS](https://nextjs.org/)
- [Emotion](https://emotion.sh/)
- [Tailwindcss](https://tailwindcss.com/)
- [Vercel](https://vercel.com/)
- [Hygraph (Previously GraphCMS)](https://hygraph.com/)
- [GraphQL](https://www.apollographql.com/docs/react/)
- [Cypress](https://cypress.io/)
- [PlanetScale](https://planetscale.com/)
- [Prisma](https://www.prisma.io/)
- [Unified](https://unifiedjs.com/)
- [Doppler](https://www.doppler.com/)
- [Axiom](https://axiom.co/)
- [Gitlab CI](https://docs.gitlab.com/ee/ci/)

- [GrowthBook](https://www.growthbook.io/)
- [Storybook](https://storybook.js.org/)

## Architecture

```mermaid
graph LR
  subgraph PlanetScale
      DB[(MySQL)]
  end
  subgraph GraphCMS
      CMSE[Edge API]
      CMSS[Serverless API]
  end
  subgraph NextJS
      WEB[Website] -->|Apollo Client| GW[GraphQL Gateway]
      GW --->|queries| CMSE
      GW --->|mutations| CMSS
      GW --> GQL
      GQL[GraphQL API] -->|Prisma| DB
  end
```

## Features

- Lambda Functions
- Schema Stitching
- Theming and Dark Mode
- Internationalisation

## Installation

- `pnpm install`

## Dev

- `pnpm dev` runs next server and codegen in watch Mode
- `pnpm dev:next` runs next server only
- `pnpm dev:codegen` runs graphql codegen in watch mode

### GraphQL Playground

Available at http://localhost:3000/api/graphql

### Testing

- `pnpm test -- --watch` runs jest in watch mode
- `pnpm cy:open` runs cypress in watch mode
- `pnpm sitespeed` runs custom sitespeed tests

### Dealing with Production issues

- [Axiom Logs](https://app.axiom.co/ornous-rtb1/datasets/vercel)
- [Axiom Dashboard](https://app.axiom.co/ornous-rtb1/dashboards/wr43F9hW0Fy9jd3WIP)
- [Sentry Unresolved Issues](https://ornous-0h.sentry.io/issues/?project=6515895)

### Linters

- `pnpm fmt` runs prettier formatter on all files
- `pnpm lint` runs linters, type checks, and prettier in check mode

eslint, stylelint, type checks and prettier in check mode

## Structure

- src/
  - _components_: Composable and reusable building blocks.
  - _pages_: Compose components into pages served based on filename.
  - _themes_: Themes define color schemes, typography and icons

## Tests

- Unit
  - Jest
  - React-testing-library
- Integration
  - Cypress
  - Jest
- UAT
  - Cypress
- Performance
  - Sitespeed.io
- Visual Regression
  - Percy.io
  -
