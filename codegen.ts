import type { CodegenConfig } from '@graphql-codegen/cli'
import { writeFileSync } from 'fs'
import yaml from 'yaml'

const addCodegenWarning = {
  add: {
    content: [
      '// @ts-nocheck',
      '// This file is auto generated. Do not modify',
    ],
  },
}

const config: CodegenConfig = {
  emitLegacyCommonJSImports: false,
  ignoreNoDocuments: true,
  generates: {
    // './graphql.schema.json': {
    //   schema: 'http://localhost:3000/api/graphql',
    //   plugins: ['introspection'],
    // },

    './src/graphql/generated/graphcms.schema.graphql': {
      schema:
        'https://api-eu-west-2.graphcms.com/v2/cl3qo78a988gh01z1gslr7cty/master',
      plugins: ['schema-ast'],
    },

    './src/graphql/generated/gql/': {
      schema: 'http://localhost:3000/api/graphql',
      // schema: 'src/graphql/schema.graphql',
      documents: ['src/graphql/operations/**/*.graphql', 'src/pages/**/*.tsx'],
      preset: 'client',
      plugins: [],
    },

    'src/graphql/generated/index.ts': {
      schema: 'src/graphql/schema.graphql',
      plugins: [addCodegenWarning, 'typescript'],
    },

    'src/graphql/generated/resolvers.ts': {
      schema: 'src/graphql/schema.graphql',
      plugins: [addCodegenWarning, 'typescript', 'typescript-resolvers'],
      config: {
        useIndexSignature: true,
        inputMaybeValue: 'T | null | undefined',
        importFrom: './',
      },
    },

    'src/graphql/generated/validators.ts': {
      schema: 'src/graphql/schema.graphql',
      plugins: [addCodegenWarning, 'typescript-validation-schema'],
      config: {
        schema: 'zod',
        strictScalars: true,
        directives: {
          constraint: {
            pattern: 'regex',
            minLength: 'min',
            // Replace $1 with specified `startsWith` argument value of the constraint directive
            startsWith: ['regex', '/^$1/', 'message'],
            format: {
              email: 'email',
            },
          },
        },
        scalars: {
          // TODO review temporary mappings
          RGBATransparency: 'z.string()',
          RichTextAST: 'z.string()',
          RGBAHue: 'z.string()',
          Long: 'z.number()',
          Json: 'z.object()',
          Hex: 'z.string()',
          Date: 'z.date()',
          DateTime: 'z.date()',
          Email: 'z.string().email()',
        },
        importFrom: './',
      },
    },
  },
}

writeFileSync('generated-codegen.yml', yaml.stringify(config))

export default config
