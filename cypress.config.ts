import { defineConfig } from 'cypress'

export default defineConfig({
  projectId: 'ornous-next',
  chromeWebSecurity: false,
  videoUploadOnPasses: true,
  blockHosts: ['*.google-analytics.com'],
  scrollBehavior: 'center',
  e2e: {
    setupNodeEvents(on, config) {
      return config
    },
    baseUrl: 'http://localhost:3000',
    // baseUrl: 'https://ornous.vercel.app',
  },
})
