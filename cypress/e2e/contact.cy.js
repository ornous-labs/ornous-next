describe('Contact Form', () => {
  it('Can send a message', () => {
    cy.visit('/contact')

    cy.fixture('contact-entry').then(([{ name, email, message }]) => {
      cy.get('#name').type(name)
      cy.get('#email').type(email)
      cy.get('#message').type(message)

      cy.get('button').contains('Send').click()
      cy.contains('Delightful Success Message')
    })
  })
})
