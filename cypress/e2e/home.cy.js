describe('The Home Page', () => {
  context('SEO', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    it('Title is correct', () => {
      cy.title().should(
        'contain',
        'Welcome to my blog and personal website! | Ornous',
      )
    })

    it("Description is set and isn't too long", () => {
      cy.get('meta[name="description"]')
        .should('have.prop', 'content')
        .and('have.length.of.at.most', 158)
    })

    it('Has one level one heading', () => {
      cy.get('h1').should('have.length.of', 1)
    })

    it('Has at least one level two heading', () => {
      cy.get('h2').should('have.length.of.at.least', 1)
    })
  })

  context('Latest Blog Articles', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    it('Has a heading', () => {
      cy.contains('Popular Blog Articles')
    })

    it('Shows three article teasers max', () => {
      cy.contains('Popular Blog Articles')
        .siblings('ul')
        .children('li')
        .should('have.length.of.at.most', 3)
        .each((teaser) => {
          expect(teaser.find('a')).to.have.prop('href').and.to.contain('/blog/')
        })
    })
  })
})
