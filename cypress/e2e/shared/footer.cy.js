describe('Footer', () => {
  context('Social Links', () => {
    // TODO consider getting these out of config
    const socialLinks = [
      { label: 'LinkedIn', url: 'https://www.linkedin.com/in/ornous/' },
      { label: 'Mastodon', url: 'https://fosstodon.org/@ornous' },
      { label: 'GitLab', url: 'https://gitlab.com/ornous' },
      { label: 'GoodReads', url: 'https://goodreads.com/ornous' },
      { label: 'RSS Feed', url: Cypress.config('baseUrl') + '/feed' },
    ]

    beforeEach(() => {
      cy.visit('/')
    })

    it('Greets with Stay in Touch', () => {
      cy.get('footer').contains('Stay in touch')
    })

    it(`Links to each social link`, () => {
      socialLinks.forEach(({ label, url }) => {
        cy.get('footer').contains(label).should('have.prop', 'href', url)
      })
    })
  })

  context('Copyright', () => {
    beforeEach(() => {
      cy.visit('/blog')
    })

    it('Displays the current year', () => {
      cy.get('footer > div').should('contain', new Date().getFullYear())
    })

    it('Links to the homepage', () => {
      // TODO remove force once scroll jank bug investigated
      cy.get('footer > div a').contains('Ousmane Ndiaye').click({ force: true })
      cy.location('pathname').should('equal', '/')
    })
  })
})
