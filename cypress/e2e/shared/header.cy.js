describe('Header', () => {
  const navLinks = [
    { label: 'home', path: '/' },
    { label: 'blog', path: '/blog' },
    { label: 'resume', path: '/resume' },
    { label: 'about', path: '/about-me' },
    { label: 'contact', path: '/contact' },
  ]

  describe('Nav Menus', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    context('720p resolution', () => {
      beforeEach(() => {
        cy.viewport(1280, 720)
      })

      it('Displays full header', () => {
        cy.get('nav #menu').should('be.visible')
        cy.get('[data-test="open-menu"]').should('not.be.visible')
      })

      it('Shows Main Nav Links', () => {
        cy.injectAxe()
        navLinks.forEach(({ label, path }) => {
          cy.get('[data-testid=menu]').contains(label).click()

          cy.location('pathname').should('equal', path)
          cy.checkA11y({
            exclude: ['.bar', '.spinner'],
          })
        })
      })
    })

    context('Iphone-5 resolution', () => {
      beforeEach(() => {
        cy.injectAxe()
        cy.viewport('iphone-5')
      })

      it('Toggles mobile menu visibility on or off', () => {
        cy.get('nav #menu').should('not.be.visible')

        cy.get('[data-test=open-menu]')
          .should('be.visible')
          .find('svg.fa-bars')
          .click()

        cy.get('nav #menu').should('be.visible')
        cy.checkA11y()

        cy.get('[data-test=open-menu]')
          .should('be.visible')
          .find('svg.fa-xmark')
          .click()

        cy.get('nav #menu').should('not.be.visible')
      })

      describe('Nav Links', () => {
        beforeEach(() => {
          cy.injectAxe()
          cy.viewport('iphone-5')
        })

        it(`Shows Navigation in Hamburger menu`, () => {
          navLinks.forEach(({ label, path }) => {
            cy.get('[data-test="open-menu"]').find('svg.fa-bars').click()
            cy.get('nav').contains(label).click()

            cy.location('pathname').should('equal', path)
            cy.checkA11y({
              exclude: ['.bar', '.spinner'],
            })
          })
        })
      })
    })
  })

  describe('Language Switcher', () => {
    it('Switches Language to French', () => {
      cy.visit('/')

      cy.location('pathname').should('equal', `/`)

      cy.findByRole('banner').within(() => {
        cy.findByRole('button', { name: 'français' }).should('not.exist')

        cy.findByRole('button', { name: 'English' })
          .should('be.visible')
          .click()

        cy.findByRole('button', { name: 'français' }).click()

        cy.findByRole('button', { name: 'français' }).should('exist')

        cy.findByRole('button', { name: 'English' }).should('not.exist')
      })

      cy.location('pathname').should('equal', `/fr`)
    })
  })

  describe('Theme Switcher', () => {
    it('Switches to dark theme', () => {
      cy.visit('/')

      cy.findByRole('banner').within(() => {
        cy.findByRole('button', { name: 'Theme' }).should('be.visible').click()

        cy.findByRole('dialog', { name: 'Theme Switcher' }).within(() => {
          cy.findByRole('button', { name: /Daring Darling/i }).click()
        })
      })

      cy.get('html').should('have.class', 'daring-darling')
    })
  })
})
