import { pathsToModuleNameMapper, type JestConfigWithTsJest } from 'ts-jest'
import { loadEnvConfig } from '@next/env'

import { compilerOptions } from './tsconfig.json'

loadEnvConfig('./')

const customJestConfig: JestConfigWithTsJest = {
  verbose: true,
  displayName: 'Ornous Blog',
  resetMocks: true,
  extensionsToTreatAsEsm: ['.ts', '.tsx'],
  setupFilesAfterEnv: ['<rootDir>/test/setup.ts'],
  moduleDirectories: ['node_modules', 'src', 'test'],
  roots: ['<rootDir>/src', '<rootDir>/test'],
  testEnvironment: 'jest-environment-jsdom',
  testEnvironmentOptions: {
    url: 'http://localhost:3000/',
    omitJSDOMErrors: true,
  },
  moduleNameMapper: {
    // Handle CSS imports (with CSS modules)
    // https://jestjs.io/docs/webpack#mocking-css-modules
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',

    // Handle CSS imports (without CSS modules)
    '^.+\\.(css|sass|scss)$': '<rootDir>/test/__mocks__/styleMock.ts',

    // Handle image imports
    // https://jestjs.io/docs/webpack#handling-static-assets
    '^.+\\.(png|jpg|jpeg|gif|webp|avif|ico|bmp)$': `<rootDir>/test/__mocks__/fileMock.ts`,
    '^.+\\.svg$': '<rootDir>/test/__mocks__/svg.ts',

    '@fontsource(-variable)?/.*$': '<rootDir>/test/__mocks__/typeface.ts',

    // Handle module aliases
    ...pathsToModuleNameMapper(compilerOptions.paths, { prefix: '<rootDir>/' }),

    // '\\.(css|less)$': '<rootDir>/test/__mocks__/styleMock.ts',
    // unified: '<rootDir>/node_modules/unified/lib/index.js',
    // 'uuid': '<rootDir>/node_modules/.pnpm/uuid@8.3.2/node_modules/uuid/dist/esm-browser/index.js',
    // 'bail': '<rootDir>/node_modules/unified/lib/index.js',
  },
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/.next/'],
  transform: {
    // Use jest-transform-graphql for graphql documents
    '\\.(gql|graphql)$': '@graphql-tools/jest-transform',
    // Use babel-jest to transpile tests with the next/babel preset
    // https://jestjs.io/docs/configuration#transform-objectstring-pathtotransformer--pathtotransformer-object
    // '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.[tj]sx?$': [
      'ts-jest',
      { useESM: true, diagnostics: false, babelConfig: true },
    ],
  },
  transformIgnorePatterns: [
    // `<rootDir>/node_modules/.pnpm/(?!${transformPackages.join(`|`)})`,
    // `<rootDir>/node_modules/.pnpm/`,
    `<rootDir>/node_modules/.pnpm/(?!globby|slash)`,
    // '<rootDir>/node_modules/',
    '<rootDir>/.next/',
    // `<rootDir>/test/setup.ts`,
  ],
  coverageProvider: 'v8',
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules',
    '!src/graphql/generated/**/*',
    '!src/stories/**/*',
    '!<rootDir>/src/middleware.ts',

    '!<rootDir>/out/**',
    '!<rootDir>/.next/**',
    '!<rootDir>/*.config.js',
    '!<rootDir>/coverage/**',

    // '!src/components/forms/ContactForm.tsx', // Temporary: WIP
    '!src/pages/_(app|document|error).tsx', // Temporary
    '!src/content/**', // Experimental, WIP

    '!src/theme/themes/*.ts',
    '!src/utils/style.ts',
  ],
  coverageThreshold: {
    global: {
      statements: 70,
      branches: 60,
      functions: 70,
      lines: 70,
    },
  },
  collectCoverage: false,
  // reporters: [
  //   'summary',
  //   [
  //     'jest-silent-reporter',
  //     {
  //       useDots: 'true',
  //       showWarnings: 'true',
  //       showPaths: 'true',
  //     },
  //   ],
  // ],
  snapshotSerializers: ['@emotion/jest/serializer'],
}

/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
export default customJestConfig
// export default createJestConfig(customJestConfig)
