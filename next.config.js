// @ts-check
import { withSentryConfig } from '@sentry/nextjs'
import { withAxiom } from 'next-axiom'
import withNextTranslate from 'next-translate-plugin'
import createBundleAnalyzer from '@next/bundle-analyzer'

const withBundleAnalyzer = createBundleAnalyzer({
  enabled: process.env.ANALYZE === 'true',
  openAnalyzer: process.env.CI !== 'true',
})

/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: { ignoreDuringBuilds: true },
  typescript: { ignoreBuildErrors: true },
  reactStrictMode: true,
  poweredByHeader: false,
  i18n: {
    locales: ['en', 'fr', 'it', 'es'],
    defaultLocale: 'en',
  },
  experimental: {
    appDir: false,
  },
  images: {
    domains: ['media.graphassets.com', 'localhost'],
    deviceSizes: [320, 480, 640, 750, 828, 1080],
  },
  sentry: {
    hideSourceMaps: false,
  },
  webpack: (config, { isServer }) => {
    config.experiments.topLevelAwait = true

    config.module.rules.push({
      test: /\.(gql|graphql)$/,
      exclude: /node_modules/,
      loader: 'graphql-tag/loader',
    })

    config.module.rules.push({
      test: /\.svg$/,
      issuer: /\.tsx?$/,
      use: ['@svgr/webpack'],
    })

    if (!isServer) {
      config.resolve.fallback = { fs: false, module: false }
    }

    return config
  },
  async rewrites() {
    return {
      beforeFiles: [
        { source: '/atom.xml', destination: '/rss/atom.xml' },
        { source: '/feed', destination: '/rss/feed.xml' },
        { source: '/feed.json', destination: '/rss/feed.json' },
      ],
      afterFiles: [],
      fallback: [],
    }
  },
}

const plugins = [
  withBundleAnalyzer,
  withAxiom,
  withNextTranslate,

  // For all available options, see:
  // https://github.com/getsentry/sentry-webpack-plugin#options.
  [withSentryConfig, { silent: true }],
]

const config = plugins.reduce((config, plugin) => {
  if (typeof plugin === 'function') return plugin(config)
  if (typeof plugin === 'object') return plugin[0](config, plugin[1])
}, nextConfig)

export default config
