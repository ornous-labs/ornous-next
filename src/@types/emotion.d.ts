import '@emotion/react'
import type { IconProp } from '@fortawesome/fontawesome-svg-core'
import type { ModularScaleRatio } from 'polished/lib/types/modularScaleRatio'

declare module '@emotion/react' {
  export type ThemeMode = 'dark' | 'light'

  type Color = string | Record<number, string>

  export interface Theme {
    name: string
    displayName?: string
    mode: ThemeMode
    icon?: IconProp

    codeHighlightTheme?: string
    hljsTheme?: string

    fonts?: {
      heading: {
        family: string
        base?: string
      }

      text: {
        family: string
        base?: string
      }
    }

    spacing?: {
      ratio: ModularScaleRatio
    }

    colors: {
      primary: Color
      accent: Color
      onPrimary: Color
      secondary: Color
      onSecondary: Color
      background: Color
      shadow: Color
      text: Color
      error: Color
      onError: Color
      codeHighlightBackground: Color
    }
  }
}
