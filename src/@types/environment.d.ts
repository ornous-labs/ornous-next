declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'test' | 'production'
      DATABASE_URL: string
      SENTRY_DSN: string
      NEXT_PUBLIC_SENTRY_DSN: string
      GRAPHCMS_URL: string
      GRAPHCMS_TOKEN: string
      NEXT_PUBLIC_GRAPHQL_URL: string
      __NEXT_IMAGE_OPTS: string
    }
  }
}

export {}
