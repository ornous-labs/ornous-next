import themeSelector, { selectDefaultTheme } from './ThemeSelector'

describe('ThemeSelector', () => {
  let mode: string | null
  const themeConfig = { light: 'light-theme', dark: 'dark-theme' }

  Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: (query: string) => ({
      get matches() {
        if (!mode) return false

        return new RegExp(`prefers-color-scheme: ${mode}`).test(query)
      },
    }),
  })

  beforeEach(() => {
    mode = null
    localStorage.removeItem('theme')
    document.documentElement.classList.value = ''
  })

  it('Sets body class name to light theme if user has no preference', () => {
    selectDefaultTheme(themeConfig)

    expect(document.documentElement.classList.value).toEqual('light-theme')
  })

  it('Sets body class name to light theme if prefered', () => {
    mode = 'light'
    selectDefaultTheme(themeConfig)

    expect(document.documentElement.classList.value).toEqual('light-theme')
  })

  it('Sets body class name to dark theme if prefered', () => {
    mode = 'dark'
    selectDefaultTheme(themeConfig)

    expect(document.documentElement.classList.value).toEqual('dark-theme')
  })

  it('Sets body class name to value from local storage if set', () => {
    localStorage.setItem('theme', 'my-imaginary-theme')

    mode = 'dark'
    selectDefaultTheme(themeConfig)

    expect(document.documentElement.classList.value).toEqual(
      'my-imaginary-theme',
    )
  })

  it('Persists selected theme to local storage', () => {
    selectDefaultTheme(themeConfig)

    expect(localStorage.getItem('theme')).toEqual('light-theme')
  })
})
