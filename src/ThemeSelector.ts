import { defaultThemes } from './theme'

interface ThemeSelectorProps {
  light: string
  dark: string
}

export const selectDefaultTheme = ({
  light: lightTheme,
  dark: darkTheme,
}: ThemeSelectorProps) => {
  const prefersDarkMode = window.matchMedia(
    '(prefers-color-scheme: dark)',
  ).matches

  const defaultTheme = prefersDarkMode ? darkTheme : lightTheme

  const persistedTheme = localStorage.getItem('theme')

  const theme = persistedTheme ?? defaultTheme

  document.documentElement.classList.add(theme)

  if (!persistedTheme) {
    localStorage.setItem('theme', theme)
  }
}

const ThemeSelector = `(${selectDefaultTheme})(${JSON.stringify(
  defaultThemes,
)})`

export default ThemeSelector
