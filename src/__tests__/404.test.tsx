import * as React from 'react'
import { render, screen } from '@test/utils'
import NotFound from '../pages/404'

jest.mock('next/router', () => ({
  useRouter() {
    return {
      asPath: '/somewhere-over-the-rainbow',
    }
  },
}))

describe('404', () => {
  it('renders correctly', () => {
    render(<NotFound />)

    expect(
      screen.getByRole('heading', {
        level: 1,
        name: /404/i,
      }),
    ).toBeDefined()

    expect(
      screen.getByRole('heading', {
        level: 2,
        // name: /this page could not be found/i,
        name: /page_not_found/i,
      }),
    ).toBeDefined()
  })
})
