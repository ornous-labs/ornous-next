import type { NextPageContext } from 'next'
import * as React from 'react'
import * as httpMocks from 'node-mocks-http'
import { act, render, screen, mockViewport } from '@test/utils'
import { post } from '@test/fixtures'
import {
  BlogArticlePage,
  getStaticProps,
  getStaticPaths,
} from '../pages/blog/[slug]'

jest.mock('next/router', () => ({
  useRouter() {
    return {
      asPath: '/blog/hello-world',
      locale: 'en',
      defaultLocale: 'en',
      query: {
        slug: 'hello-world',
      },
    }
  },
}))

describe('BlogArticle', () => {
  it('Component', async () => {
    const viewport = mockViewport({ width: '320px', height: '568px' })

    await act(() => {
      render(<BlogArticlePage data={{ post }} />)
    })

    expect(
      screen.getByRole('heading', { level: 2, name: /Category Name/i }),
    ).toBeInTheDocument()

    expect(
      screen.getByRole('heading', { level: 1, name: /Post Title/i }),
    ).toBeInTheDocument()

    expect(
      screen.getByRole('heading', { level: 1, name: /title 1/i }),
    ).toBeInTheDocument()

    expect(screen.getByText(post.description)).toBeInTheDocument()

    viewport.cleanup()
  })

  it('getStaticProps', async () => {
    expect.hasAssertions()

    const ctx: NextPageContext = {
      // @NOTE AppTree is currently just a stub
      AppTree: React.Fragment,
      pathname: '/blog/hello-graphcms',
      query: {},
      locale: 'en',
      defaultLocale: 'en',
      params: {
        slug: 'hello-graphcms',
      },
      asPath: '/blog/hello-graphcms',
      resolvedUrl: '/blog/hello-graphcms',
    }

    ctx.req = httpMocks.createRequest({})
    ctx.res = httpMocks.createResponse({})

    const result = await getStaticProps(ctx)

    expect(result).toMatchObject({
      props: {
        data: {
          post: {
            id: expect.any(String),
            description: expect.any(String),
            title: expect.any(String),
            md: expect.any(String),
            heroImage: expect.objectContaining({
              height: expect.any(Number),
              width: expect.any(Number),
              id: expect.any(String),
              url: expect.any(String),
            }),
          },
        },
      },
      revalidate: expect.any(Number),
    })
  })

  it('getStaticPaths', async () => {
    expect.hasAssertions()
    const result = await getStaticPaths({ locales: [] })

    expect(result.fallback).toEqual(false)

    result.paths?.forEach((path) => {
      expect(path).toMatchObject({
        locale: expect.any(String),
        params: {
          slug: expect.any(String),
        },
      })
    })
  })
})
