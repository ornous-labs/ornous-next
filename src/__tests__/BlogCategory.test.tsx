process.env.ENABLE_MOCKS = 'true'

import { NextPageContext } from 'next'
import * as React from 'react'
import * as httpMocks from 'node-mocks-http'
import { render, screen, mockViewport } from '@test/utils'
import { rubrique } from '@test/fixtures'
import {
  BlogCategoryList,
  getStaticProps,
  getStaticPaths,
} from '../pages/blog/category/[slug]'

jest.mock('next/router', () => ({
  useRouter() {
    return {
      asPath: '/blog/my-category',
      query: {
        slug: 'my-category',
      },
    }
  },
}))

describe('BlogCategory', () => {
  it('Component', () => {
    const viewport = mockViewport({ width: '320px', height: '568px' })

    render(<BlogCategoryList data={{ rubrique }} />)

    // TODO check that chip is marked as current instead
    // expect(
    //   screen.getByRole('heading', {
    //     level: 1,
    //     name: /My Category/i,
    //   })
    // ).toBeInTheDocument()

    const articleLink = screen.getByRole('link', { name: /Post Title/i })
    expect(articleLink).toBeInTheDocument()
    expect((articleLink as HTMLAnchorElement).href).toEqual(
      'http://localhost:3000/blog/post-title',
    )
    viewport.cleanup()
  })

  it('getStaticProps', async () => {
    expect.hasAssertions()
    const ctx: NextPageContext = {
      // @NOTE AppTree is currently just a stub
      AppTree: React.Fragment,
      pathname: '/blog/category/my-category',
      query: {},
      locale: 'en',
      defaultLocale: 'en',
      params: {
        slug: 'my-category',
      },
      asPath: '/blog/category/my-category',
      resolvedUrl: '/blog/category/my-category',
    }

    ctx.req = httpMocks.createRequest({})
    ctx.res = httpMocks.createResponse({})

    const result = await getStaticProps(ctx)

    expect(result).toMatchObject({
      props: {
        data: {
          rubrique: {
            id: expect.any(String),
            posts: expect.arrayContaining([
              expect.objectContaining({
                id: expect.any(String),
                slug: expect.any(String),
                title: expect.any(String),
                description: expect.any(String),
              }),
            ]),
          },
        },
      },
      revalidate: expect.any(Number),
    })
  })

  it('getStaticPaths', async () => {
    expect.hasAssertions()

    const result = await getStaticPaths({ locales: [] })

    expect(result.fallback).toEqual('blocking')

    result.paths?.forEach((path) => {
      expect(path).toMatchObject({
        locale: expect.any(String),
        params: {
          slug: expect.any(String),
        },
      })
    })
  })
})
