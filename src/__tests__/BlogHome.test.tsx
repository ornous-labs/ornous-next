import type { NextPageContext } from 'next'
import * as React from 'react'
import * as httpMocks from 'node-mocks-http'
import { setImmediate } from 'timers'

import { render, screen, act, mockViewport } from '@test/utils'
import { post, rubrique } from '@test/fixtures'

global.setImmediate = setImmediate

import { BlogHome, getStaticProps } from '../pages/blog'

jest.mock('next/router', () => ({
  useRouter() {
    return {
      asPath: '/blog/',
    }
  },
}))

describe('BlogHome', () => {
  it('Component', async () => {
    const viewport = mockViewport({ width: '320px', height: '568px' })

    await act(() => {
      render(<BlogHome data={{ posts: [post], rubriques: [rubrique] }} />)
    })

    expect(
      screen.getByRole('heading', { level: 1, name: 'blog:posts' }),
    ).toBeInTheDocument()

    expect(
      screen.getByRole('navigation', { name: 'blog:categories' }),
    ).toBeInTheDocument()

    viewport.cleanup()
  })

  // TODO investigate apollo network error during test
  it('getStaticProps', async () => {
    expect.hasAssertions()

    const ctx: NextPageContext = {
      // @NOTE AppTree is currently just a stub
      AppTree: React.Fragment,
      pathname: '/blog',
      query: {},
      locale: 'en',
      defaultLocale: 'en',
      params: {},
      asPath: '/blog',
      resolvedUrl: '/blog',
    }

    ctx.req = httpMocks.createRequest({})
    ctx.res = httpMocks.createResponse({})

    const result = await getStaticProps(ctx)

    expect(result).toMatchObject({
      props: {
        data: {
          posts: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(String),
              slug: expect.any(String),
              title: expect.any(String),
              localizations: expect.arrayContaining([
                expect.objectContaining({
                  id: expect.any(String),
                  locale: expect.any(String),
                  slug: expect.any(String),
                  title: expect.any(String),
                }),
              ]),
            }),
          ]),
          rubriques: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(String),
              slug: expect.any(String),
              name: expect.any(String),
              localizations: expect.arrayContaining([
                {
                  id: expect.any(String),
                  locale: expect.any(String),
                  slug: expect.any(String),
                  name: expect.any(String),
                },
              ]),
            }),
          ]),
        },
      },
      revalidate: expect.any(Number),
    })
  }, 10000)
})
