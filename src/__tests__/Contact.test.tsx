import * as React from 'react'
import { MockedProvider } from '@apollo/client/testing'

import { render, screen, userEvent } from '../../test/utils'
import { ContactDocument } from '../graphql/generated/index'
import Contact from '../pages/contact'

describe('contact', () => {
  it('Shows message on success', async () => {
    const input = {
      name: 'Jane Doe',
      email: 'jane.doe@company.com',
      message: 'Hey, how are ya?',
    }

    const mocks = [
      {
        request: {
          query: ContactDocument,
          variables: { input },
        },
        result: {
          data: {
            contact: {
              success: true,
            },
          },
        },
      },
    ]

    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Contact />
      </MockedProvider>,
    )

    const name = screen.getByRole('textbox', { name: /name/i })
    const email = screen.getByRole('textbox', { name: /email/i })
    const message = screen.getByRole('textbox', { name: /message/i })
    const send = screen.getByRole('button', { name: /send/i })

    await userEvent.type(name, input.name)
    await userEvent.type(email, input.email)
    await userEvent.type(message, input.message)

    await userEvent.click(send)

    const success = await screen.findByText('contact_success_message')

    expect(success).toBeInTheDocument()
  })

  it('Shows errors for invalid values', async () => {
    render(
      <MockedProvider mocks={[]} addTypename={false}>
        <Contact />
      </MockedProvider>,
    )

    const send = screen.getByRole('button', { name: /send/i })

    await userEvent.click(send)

    const name = screen.getByRole('textbox', { name: /name/i })
    const email = screen.getByRole('textbox', { name: /email/i })
    const message = screen.getByRole('textbox', { name: /message/i })

    await screen.findAllByRole('alert')

    expect(name).toHaveAccessibleErrorMessage('errors.required')
    expect(email).toHaveAccessibleErrorMessage('errors.invalid_email')
    expect(message).toHaveAccessibleErrorMessage('errors.required')
  })
})
