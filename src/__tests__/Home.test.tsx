import * as React from 'react'

import { act, render, screen } from '@test/utils'
import { post } from '@test/fixtures'
import Home from '../pages'

describe('Home', () => {
  it('Component', async () => {
    const data = {
      rubriques: [
        {
          promotedPosts: [post],
        },
      ],
    }

    await act(() => {
      render(<Home data={data} />)
    })

    expect(
      screen.getByRole('heading', {
        level: 1,
        name: /home:title/i,
      }),
    ).toBeDefined()

    expect(
      screen.getByRole('heading', {
        level: 2,
        name: /home:promoted_posts/i,
      }),
    ).toBeDefined()

    expect(
      screen.getByRole('link', {
        name: /post title/i,
      }).href,
    ).toMatch('/blog/post')

    expect(screen.getByText(/reading_time {"count":6}/)).toBeDefined()
  })
})
