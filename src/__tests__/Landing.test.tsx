process.env.ENABLE_MOCKS = 'true'

import { NextPageContext } from 'next'
import * as httpMocks from 'node-mocks-http'
import * as React from 'react'
import { act, render, screen } from '@test/utils'
import { image } from '@test/fixtures'
import Router from 'next/router'
Router.defaultLocale = 'en'
import { LandingPage, getStaticProps } from '../pages/[slug]'

describe('LandingPage', () => {
  it('Component', async () => {
    const data = {
      page: {
        id: '1',
        slug: 'landing-page',
        title: 'Landing Page',
        description: "My Landing Page's Description",
        content: '<h2>Moon Landing</h2>',
        heroImage: [image],
      },
    }

    await act(() => {
      render(<LandingPage data={data} />)
    })

    expect(
      screen.getByRole('heading', {
        level: 1,
        name: /Landing Page/i,
      }),
    ).toBeInTheDocument()

    expect(
      screen.getByRole('heading', {
        level: 2,
        name: /Moon Landing/i,
      }),
    ).toBeInTheDocument()

    expect(screen.getByText(data.page.description)).toBeInTheDocument()
  })

  it('getStaticProps', async () => {
    expect.hasAssertions()
    const ctx: NextPageContext = {
      // @NOTE AppTree is currently just a stub
      AppTree: React.Fragment,
      pathname: '/about-me',
      query: {},
      params: {
        slug: 'about-me',
      },
      asPath: '/about-me',
      resolvedUrl: '/about-me',
    }

    ctx.req = httpMocks.createRequest({})
    ctx.res = httpMocks.createResponse({})

    const result = await getStaticProps(ctx)

    expect(result).toMatchObject({
      props: {
        data: {
          page: {
            id: expect.any(String),
            description: expect.any(String),
            title: expect.any(String),
            md: expect.any(String),
            heroImage: expect.arrayContaining([
              expect.objectContaining({
                height: expect.any(Number),
                width: expect.any(Number),
                id: expect.any(String),
                url: expect.any(String),
              }),
            ]),
          },
        },
      },
      revalidate: expect.any(Number),
    })
  })
})
