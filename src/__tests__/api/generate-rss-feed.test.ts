/**
 * @jest-environment node
 */
import { testApiHandler } from 'next-test-api-route-handler'
import endpoint, { config } from '../../pages/api/generate-rss-feed'
import type { PageConfig } from 'next'

const handler: typeof endpoint & { config?: PageConfig } = endpoint
handler.config = config as PageConfig

describe('Handler: Generate RSS Feed', () => {
  it('errors out if missing secret', async () => {
    await testApiHandler<{ message: string }>({
      handler,
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(401)

        const { message } = await res.json()
        expect(message).toBe('Invalid token')
      },
    })
  })

  it('returns revalidate true on success', async () => {
    await testApiHandler<{ status: 'success' }>({
      handler,
      params: { secret: 'testtoken' },
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(200)

        const { status } = await res.json()
        expect(status).toBe('success')
      },
    })
  })
})
