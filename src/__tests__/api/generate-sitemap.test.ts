/**
 * @jest-environment node
 */
import { testApiHandler } from 'next-test-api-route-handler'
import endpoint, { config } from '../../pages/api/generate-sitemap'
import type { PageConfig } from 'next'

const handler: typeof endpoint & { config?: PageConfig } = endpoint
handler.config = config as PageConfig

describe('Handler: Generate Sitemap', () => {
  it('errors out if missing secret', async () => {
    await testApiHandler({
      handler,
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(401)

        const { message } = await res.json()
        expect(message).toBe('Invalid token')
      },
    })
  })

  it('returns revalidate true on success', async () => {
    await testApiHandler({
      handler,
      params: { secret: 'testtoken' },
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(200)
      },
    })
  })
})
