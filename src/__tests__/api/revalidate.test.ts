import { testApiHandler } from 'next-test-api-route-handler'
import endpoint, { config } from '../../pages/api/revalidate'
import type { PageConfig } from 'next'

const handler: typeof endpoint & { config?: PageConfig } = endpoint
handler.config = config as PageConfig

describe('Handler: Revalidate', () => {
  it('errors out if missing secret', async () => {
    await testApiHandler<{ message: string }>({
      handler,
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(401)

        const { message } = await res.json()
        expect(message).toBe('Invalid token')
      },
    })
  })

  it('errors out if missing path', async () => {
    await testApiHandler<{ message: string }>({
      handler,
      requestPatcher: (req) => (req.url = '/api/revalidate?secret=testtoken'),
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(400)

        const { message } = await res.json()
        expect(message).toBe('Missing path to revalidate')
      },
    })
  })

  it('errors out if array passed as path', async () => {
    await testApiHandler<{ message: string }>({
      handler,
      requestPatcher: (req) => (req.url = '/api/revalidate?secret=testtoken'),
      params: { path: ['/path1', 'path2'] },
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(400)

        const { message } = await res.json()
        expect(message).toBe('Can only revalidate one path at a time')
      },
    })
  })

  it('returns error if revalidation failed', async () => {
    await testApiHandler<{ message: string }>({
      handler,
      requestPatcher: (req) => (req.url = '/api/revalidate?secret=testtoken'),
      params: { path: '/blog/hello-graphcms' },
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(500)

        const { message } = await res.json()
        expect(message).toBe('Error revalidating')
      },
    })
  })

  it('returns revalidate true on success', async () => {
    await testApiHandler<{ revalidated: boolean }>({
      handler: (req, res) => {
        res.revalidate = () => {
          return Promise.resolve()
        }
        return handler(req, res)
      },
      requestPatcher: (req) => (req.url = '/api/revalidate?secret=testtoken'),
      params: { path: '/blog/hello-graphcms' },
      test: async ({ fetch }) => {
        const res = await fetch({ method: 'GET' })
        expect(res.status).toBe(200)

        const { revalidated } = await res.json()
        expect(revalidated).toBe(true)
      },
    })
  })
})
