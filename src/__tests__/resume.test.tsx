import * as React from 'react'

import { act, render, screen } from '../../test/utils'
import Resume from '../pages/resume'

describe('resume', () => {
  it('Component', async () => {
    act(() => {
      render(<Resume />)
    })

    expect(screen.getByRole('heading', { level: 1 })).toBeInTheDocument()
    expect(
      screen.getByRole('heading', {
        level: 2,
        name: 'skills',
      }),
    ).toBeInTheDocument()
    expect(
      screen.getByRole('heading', {
        level: 2,
        name: 'job_history',
      }),
    ).toBeInTheDocument()
  })
})
