import styled from '@emotion/styled'
import { readableColor } from 'polished'
import type { Color } from '@graphql/generated'

export const Chip = styled.div<{ colour?: Color['hex'] }>`
  position: relative;
  isolation: isolate;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0.1rem;
  padding: 0.35em 1.4em;
  min-inline-size: max-content;
  line-height: 1;
  border-radius: 4px;
  transition: transform var(--transition-xfast) ease-out;
  background-color: rgb(var(--color-primary-300));
  overflow: hidden;
  box-shadow: var(--shadow-xs);
  font-size: var(--font-text-xs);
  font-weight: 400;
  text-transform: uppercase;
  letter-spacing: 0.02em;

  a {
    color: rgb(var(--color-background-300));
  }

  ${({ colour }) =>
    colour
      ? `
      a::before,
      a::after {
        position: absolute;
        content: '';
        top: 0;
        bottom: 0;
        width: 7.5%;
        background-color: ${colour};
        z-index: -1;

        @media (prefers-reduced-motion: no-preference) {
          transition: transform var(--transition-fast) ease-out;
        }
      }

      a::before {
        left: 0;
      }

      a::after {
        right: 0;
      }
  `
      : null};

  a:is(:hover, :focus, [aria-current='true']) {
    text-decoration: dotted underline;
    outline: none;
    ${({ colour }) => colour && `color: ${readableColor(colour)}`};

    &::before,
    &::after {
      transform: scaleX(13);
    }
  }
`
