import type { FC } from 'react'

interface Props {
  content: string
}

const Content: FC<Props> = ({ content }) => {
  return <div dangerouslySetInnerHTML={{ __html: content }} />
}

export { Content }
