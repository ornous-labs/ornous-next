import * as React from 'react'

import { render, screen } from '@test/utils'

import Wrapper from '../Wrapper'

describe('Wrapper', () => {
  it('should render correctly', () => {
    render(<Wrapper>Label</Wrapper>)

    expect(screen.getByRole('contentinfo')).toHaveTextContent('Label')
  })
})
