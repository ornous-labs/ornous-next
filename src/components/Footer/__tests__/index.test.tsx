import { render, screen, cleanup } from '@test/utils'

import Footer, { Copyright, Nav } from '..'

describe('Footer', () => {
  afterEach(cleanup)

  it('should be defined', () => {
    expect(Footer).toBeDefined()
  })

  describe('Nav', () => {
    it('should be defined', () => {
      expect(Nav).toBeDefined()
    })

    it('should render correctly', () => {
      render(<Nav>Label</Nav>)

      expect(screen.getByRole('navigation')).toHaveTextContent('Label')
    })
  })

  describe('Copyright', () => {
    it('should link to homepage', () => {
      render(<Copyright />)

      const link: HTMLAnchorElement = screen.getByRole('link', {
        name: 'Ousmane Ndiaye',
      })
      expect(link.href).toEqual('http://localhost:3000/')
    })

    it('should be defined', () => {
      expect(Copyright).toBeDefined()
    })

    it('should render the current year', () => {
      const { container } = render(<Copyright />)

      expect(container.innerHTML).toEqual(
        expect.stringContaining(new Date().getFullYear().toString()),
      )
    })
  })
})
