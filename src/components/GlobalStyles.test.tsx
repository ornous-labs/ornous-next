import React from 'react'

import { render } from '@test/utils'

import GlobalStyles from './GlobalStyles'

describe('GlobalStyles', () => {
  it('unmounts cleanly', () => {
    const { unmount } = render(<GlobalStyles />)
    unmount()
    expect(document.head.children).toHaveLength(0)
  })
})
