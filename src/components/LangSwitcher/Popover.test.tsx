import Router from 'next/router'

import { render, screen, act, fireEvent } from '../../../test/utils'

import { LangSwitcherPopover } from './'

Router.locale = 'fr'
Router.locales = ['en', 'fr']

describe('LangSwitcher', () => {
  it('Only shows current locale as trigger', () => {
    render(<LangSwitcherPopover />)

    expect(
      screen.queryByRole('button', {
        name: /English/,
      }),
    ).not.toBeInTheDocument()
    expect(
      screen.queryByRole('button', {
        name: /français/,
      }),
    ).not.toBeInTheDocument()
  })

  it('Switches locale on click', async () => {
    act(() => {
      render(<LangSwitcherPopover />)
    })

    const triggerLink = await screen.findByRole('button')

    fireEvent.click(triggerLink)

    const en = screen.getByRole('button', { name: /English/ })
    const fr = screen.getByRole('button', { name: /français/ })

    expect(fr).toBeInTheDocument()
    expect(en).toBeInTheDocument()

    fireEvent.click(en)

    expect(await screen.findByText(/English/)).toBeInTheDocument()
    expect(Router.locale).toEqual('en')
  })
})
