import Router from 'next/router'

import { act, render, screen, fireEvent } from '@test/utils'

import { LangSwitcher } from './'

Router.locale = 'en'
Router.locales = ['en', 'fr']
Router.defaultLocale = 'en'

describe('LangSwitcher', () => {
  it('renders correctly', () => {
    render(<LangSwitcher />)

    expect(screen.getByRole('button', { name: /English/i })).toBeInTheDocument()
  })

  it('switches locales while preserving path on click', async () => {
    Router.push('/initial-path')

    await act(() => {
      render(<LangSwitcher />)
    })

    const frLink = screen.getByRole('button', { name: /français/ })
    const enLink = screen.getByRole('button', { name: /English/ })

    fireEvent.click(frLink)

    expect(Router).toMatchObject(
      expect.objectContaining({
        locale: 'fr',
        asPath: '/initial-path',
      }),
    )

    fireEvent.click(enLink)
    expect(Router).toMatchObject(
      expect.objectContaining({
        locale: 'en',
        asPath: '/initial-path',
      }),
    )
  })
})
