import { render, screen, within } from '@test/utils'
import Layout from './Layout'

describe('layout', () => {
  it('has a home link in the footer', () => {
    render(<Layout>Content</Layout>)

    const footer = within(screen.getByRole('contentinfo'))
    const homeLink = footer.getByRole('link', { name: 'Ousmane Ndiaye' })

    expect(homeLink).toHaveAttribute('href', '/')
  })
})
