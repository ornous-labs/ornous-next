import Router from 'next/router'
import { render, screen, fireEvent, act } from '../../../test/utils'

import Hamburger from './Hamburger'
import { createRef } from 'react'

describe('Hamburger', () => {
  it('opens menu on click', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(<Hamburger menuRef={menuRef} isOpen={false} setIsOpen={setIsOpen} />)

    act(() => {
      fireEvent.click(screen.getByTestId('open-menu', { hidden: true }))
    })

    expect(setIsOpen).toHaveBeenNthCalledWith(1, true)
  })

  it('closes menu on click', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(<Hamburger menuRef={menuRef} isOpen={true} setIsOpen={setIsOpen} />)

    act(() => {
      fireEvent.click(screen.getByTestId('open-menu', { hidden: true }))
    })

    expect(setIsOpen).toHaveBeenNthCalledWith(1, false)
  })

  it('closes menu on route change', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(<Hamburger menuRef={menuRef} isOpen={true} setIsOpen={setIsOpen} />)

    act(() => {
      Router.events.emit('routeChangeStart')
    })

    expect(setIsOpen).toHaveBeenNthCalledWith(1, false)
  })

  it('closes menu on click outside', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(
      <div>
        <button>Outside</button>
        <button ref={menuRef}>Inside</button>
        <Hamburger menuRef={menuRef} isOpen={true} setIsOpen={setIsOpen} />
      </div>,
    )

    fireEvent.mouseDown(screen.getByRole('button', { name: /Inside/ }))

    expect(setIsOpen).toHaveBeenCalledTimes(0)

    fireEvent.mouseDown(screen.getByRole('button', { name: /Outside/ }))

    expect(setIsOpen).toHaveBeenNthCalledWith(1, false)
  })

  it('closes menu on touch outside', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(
      <div>
        <button>Outside</button>
        <button ref={menuRef}>Inside</button>
        <Hamburger menuRef={menuRef} isOpen={true} setIsOpen={setIsOpen} />
      </div>,
    )

    act(() => {
      fireEvent.touchStart(screen.getByRole('button', { name: /Inside/ }))
    })

    expect(setIsOpen).toHaveBeenCalledTimes(0)

    act(() => {
      fireEvent.touchStart(screen.getByRole('button', { name: /Outside/ }))
    })

    expect(setIsOpen).toHaveBeenNthCalledWith(1, false)
  })

  it('closes menu on escape key down', () => {
    const setIsOpen = jest.fn()
    const menuRef = createRef<HTMLButtonElement>()

    render(<Hamburger menuRef={menuRef} isOpen={true} setIsOpen={setIsOpen} />)

    act(() => {
      fireEvent.keyDown(document, { key: 'Escape' })
    })

    expect(setIsOpen).toHaveBeenNthCalledWith(1, false)
  })
})
