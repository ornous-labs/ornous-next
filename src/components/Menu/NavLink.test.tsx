import * as React from 'react'

import { render, screen } from '../../../test/utils'

import NavLink from './NavLink'

describe('NavLink', () => {
  it('renders correctly', () => {
    render(<NavLink href="https://www.ornous.com">Label</NavLink>)

    const link = screen.getByRole('link', { name: 'Label' })

    expect(link).toHaveAttribute('href', 'https://www.ornous.com')
  })

  it('adds aria-current and active class name based on active flag', () => {
    render(
      <React.Fragment>
        <NavLink href="https://www.ornous.com" active>
          active
        </NavLink>
        <NavLink href="/">inactive</NavLink>
      </React.Fragment>,
    )

    const activeLink = screen.getByRole('link', { name: 'active' })
    const inactiveLink = screen.getByRole('link', { name: 'inactive' })

    expect(activeLink).toHaveClass('active')
    expect(activeLink).toHaveAttribute('aria-current', 'page')

    expect(inactiveLink).not.toHaveClass('active')
    expect(inactiveLink).not.toHaveAttribute('aria-current', false)
  })
})
