import * as React from 'react'
import { render, cleanup, screen, fireEvent, within, act } from '@test/utils'
import useMenu from '../../hooks/useMenu'

jest.mock('../../hooks/useMenu')

import Menu from '.'

describe('Menu', () => {
  const defaultUseMenuValue = {
    menuItems: [
      { name: 'link 1', path: '/link/1' },
      { name: 'link 2', path: '/link/2' },
    ],
    isOpen: false,
    setIsOpen: jest.fn(),
    close: jest.fn(),
    toggle: jest.fn(),
  }

  beforeEach(() => {
    // @ts-expect-error jest.mock adds this functionality
    useMenu.mockReturnValue(defaultUseMenuValue)
  })

  afterEach(cleanup)

  it('renders menu items correctly', async () => {
    await act(() => {
      render(<Menu />)
    })

    const menu = within(screen.getByRole('navigation'))

    expect(menu.getByRole('link', { name: 'link 1' })).toHaveAttribute(
      'href',
      '/link/1',
    )
    expect(menu.getByRole('link', { name: 'link 2' })).toHaveAttribute(
      'href',
      '/link/2',
    )
  })

  it('open menu with a click', async () => {
    await act(() => {
      render(<Menu />)
    })

    await act(() => {
      fireEvent.click(screen.getByTestId('open-menu', { hidden: true }))
    })

    expect(useMenu().setIsOpen).toHaveBeenNthCalledWith(1, true)
  })

  it('closes menu with a click', async () => {
    // @ts-expect-error jest.mock adds this functionality
    useMenu.mockReturnValue({ ...defaultUseMenuValue, isOpen: true })

    await act(() => {
      render(<Menu />)
    })

    await act(() => {
      fireEvent.click(screen.getByTestId('open-menu', { hidden: true }))
    })

    expect(useMenu().setIsOpen).toHaveBeenNthCalledWith(1, false)
  })
})
