import { render, screen } from '@test/utils'

import { TagList } from './'

describe('Tag', () => {
  it('renders correctly', () => {
    const tags = [
      { slug: 'next', title: 'nextjs' },
      { slug: 'react', title: 'react' },
    ]

    render(<TagList tags={tags} />)

    expect(screen.getByRole('link', { name: /nextjs/ })).toHaveAttribute(
      'href',
      '/tag/next',
    )

    expect(screen.getByRole('link', { name: /react/ })).toHaveAttribute(
      'href',
      '/tag/react',
    )
  })
})
