import type { FC } from 'react'
import { css } from '@emotion/react'
import styled from '@emotion/styled'

import type { Tag as TagType } from '@graphql/generated'
import Link from '@components/Link'

const List = styled.ul`
  display: flex;
  flex-flow: row wrap;
  gap: 0;
  list-style-type: none;
  margin: 0;
  padding: 0;
  text-indent: 0;

  // restores alignment caused by tag padding during hover without layout shift
  --padding-inline: 0.4rem;

  position: relative;
  left: -0.5rem;

  li {
    display: inline;
    width: max-content;
    margin: 0;
    padding: 0;

    a {
      position: relative;
      gap: 0;
      margin: 0;

      &::before {
        position: relative;
        left: 0;
        top: 0;
        content: '#';
      }
    }
  }
`

const Grid = styled(List)`
  justify-content: space-around;
`

type TagData = Pick<TagType, 'slug' | 'title'>

export const TagList: FC<{ tags: TagData[] }> = ({ tags }) => {
  return (
    <List>
      {tags.map((tag) => (
        <li key={tag.slug}>
          <Tag {...tag} />
        </li>
      ))}
    </List>
  )
}

export const TagGrid: FC<{ tags: TagData[] }> = ({ tags }) => {
  return (
    <Grid>
      {tags.map((tag) => (
        <li key={tag.slug}>
          <Tag {...tag} />
        </li>
      ))}
    </Grid>
  )
}

export const Tag: FC<TagData> = ({ title, slug }) => {
  const styles = css`
    padding: 0.2rem var(--padding-inline);
    text-decoration: none;
    border: 1px solid transparent;
    border-radius: 5px;
    color: rgb(var(--color-primary-50));
    font-weight: bold;
    letter-spacing: 0.9px;
    transition:
      color var(--transition-xfast) ease-in-out,
      background-color var(--transition-xfast) ease-in-out,
      border-color var(--transition-xfast) ease-in-out;

    &:hover,
    &:focus-within {
      color: rgb(var(--color-background-500));
      background-color: rgb(var(--color-primary-50));
      border-color: rgb(var(--color-primary-500));
      outline: none;
    }
  `

  return (
    <Link href={`/tag/${slug}`} css={styles}>
      {title}
    </Link>
  )
}
