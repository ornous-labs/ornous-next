import * as React from 'react'
import { fireEvent, render, screen, act } from '../../../test/utils'

import { ThemeSwitcherPopover } from './'

describe('ThemeSwitcher', () => {
  it('Only shows current locale as trigger', () => {
    render(<ThemeSwitcherPopover />)

    expect(
      screen.queryByRole('button', {
        name: /karmic koala/,
      }),
    ).not.toBeInTheDocument()
    expect(
      screen.queryByRole('button', {
        name: /warm antique/,
      }),
    ).not.toBeInTheDocument()
  })

  it('Switches theme on click', async () => {
    act(() => {
      render(<ThemeSwitcherPopover />)
    })

    const triggerLink = await screen.findByRole('button')

    await act(() => {
      fireEvent.click(triggerLink)
    })

    const karmicKoala = screen.getByRole('button', { name: /karmic koala/i })
    const warmAntique = screen.getByRole('button', { name: /warm antique/i })

    expect(karmicKoala).toBeInTheDocument()
    expect(warmAntique).toBeInTheDocument()

    await act(() => {
      fireEvent.click(warmAntique)
    })

    expect(localStorage.getItem('theme')).toEqual('warm-antique')
    expect(document.documentElement.classList.value).toEqual('warm-antique')
  })
})
