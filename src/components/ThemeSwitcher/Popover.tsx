import { Fragment, type FC, useContext, useState } from 'react'
import { css } from '@emotion/react'
import useTranslation from 'next-translate/useTranslation'

import { ThemeSwitcher } from '.'
import { themes } from '../../theme'
import { ThemeContext } from '../../theme/Context'
import { PopoverTrigger, PopoverContent, usePopover } from '@components/Popover'
import { Icon, StackIcons } from '@components/Icon'

const ThemeSwitcherPopover: FC = () => {
  const { t } = useTranslation('common')
  const { theme: themeName } = useContext(ThemeContext)

  const theme = themes[themeName]

  const [isOpen, setIsOpen] = useState(false)
  const {
    isMounted,
    transitionStyles,
    x,
    y,
    strategy,
    refs,
    getReferenceProps,
    getFloatingProps,
  } = usePopover({ isOpen, onOpenChange: setIsOpen })

  const posStyles = css({
    position: strategy,
    top: y ?? 0,
    left: x ?? 0,
    ...transitionStyles,
  })

  return (
    <Fragment>
      <PopoverTrigger
        ref={refs.setReference}
        isOpen={isOpen}
        {...getReferenceProps()}
      >
        <StackIcons>
          <Icon icon="square" fixedWidth />
          <Icon
            icon={theme.icon}
            style={{ color: theme.colors.secondary! }}
            transform="shrink-6"
            fixedWidth
          />
        </StackIcons>
        {t('theme')}
      </PopoverTrigger>
      <PopoverContent
        aria-label="Theme Switcher"
        ref={refs.setFloating}
        css={posStyles}
        isMounted={isMounted}
        {...getFloatingProps()}
      >
        <ThemeSwitcher onSwitch={() => setIsOpen(false)} />
      </PopoverContent>
    </Fragment>
  )
}

export { ThemeSwitcherPopover }
