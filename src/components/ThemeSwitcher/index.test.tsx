import * as React from 'react'

import { act, render, screen, fireEvent } from '@test/utils'

import { ThemeSwitcher } from './'
import { defaultThemes } from '../../theme'
import { ThemeContext, ThemeProvider } from '../../theme/Context'

describe('ThemeSwitcher', () => {
  const Harness = () => {
    const { theme } = React.useContext(ThemeContext)

    return <span>{theme}</span>
  }

  it('Uses default theme if local storage unset', () => {
    render(
      <ThemeProvider>
        <ThemeSwitcher />
        <Harness />
      </ThemeProvider>,
    )

    expect(
      screen.getByText(defaultThemes.light, { selector: 'span' }),
    ).toBeInTheDocument()
  })

  it('Uses theme from local storage as default if set', () => {
    localStorage.setItem('theme', defaultThemes.dark)

    render(
      <ThemeProvider>
        <ThemeSwitcher />
        <Harness />
      </ThemeProvider>,
    )

    expect(
      screen.getByText(defaultThemes.dark, { selector: 'span' }),
    ).toBeInTheDocument()
  })

  it('Switches themes on click', () => {
    render(
      <ThemeProvider>
        <ThemeSwitcher />
        <Harness />
      </ThemeProvider>,
    )

    fireEvent.click(screen.getByRole('button', { name: /daring darling/i }))

    expect(
      screen.getByText('daring-darling', { selector: 'span' }),
    ).toBeInTheDocument()

    fireEvent.click(screen.getByRole('button', { name: /warm antique/i }))

    expect(
      screen.getByText('warm-antique', { selector: 'span' }),
    ).toBeInTheDocument()
  })

  it('Persists theme selection to local storage', async () => {
    render(
      <ThemeProvider>
        <ThemeSwitcher />
        <Harness />
      </ThemeProvider>,
    )
    const localStorageSpy = jest.spyOn(Storage.prototype, 'setItem')

    const ddLink = screen.getByRole('button', { name: /daring darling/i })

    await act(() => {
      fireEvent.click(ddLink)
    })

    expect(localStorageSpy).toHaveBeenCalledWith(
      'theme',
      expect.stringMatching('daring-darling'),
    )

    const waLink = screen.getByRole('button', { name: /warm antique/i })
    await act(() => {
      fireEvent.click(waLink)
    })

    expect(localStorageSpy).toHaveBeenLastCalledWith(
      'theme',
      expect.stringMatching('warm-antique'),
    )
  })
})
