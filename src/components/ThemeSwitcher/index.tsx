import { Fragment, type FC, useCallback, useContext, useState } from 'react'
import { css, type Theme } from '@emotion/react'

import { themes } from '../../theme'
import { ThemeContext } from '../../theme/Context'
import { PopoverTrigger, PopoverContent, usePopover } from '@components/Popover'
import { List } from '@components/Footer/Nav'
import { Icon, StackIcons } from '@components/Icon'
import useTranslation from 'next-translate/useTranslation'

interface Props {
  onSwitch?(): unknown
}

const ThemeSwitcher: FC<Props> = ({ onSwitch }) => {
  const { theme: currentTheme, setTheme } = useContext(ThemeContext)

  const switchThemes = useCallback(
    (newTheme: Theme) => {
      localStorage.setItem('theme', newTheme.name)

      if (typeof setTheme !== 'undefined') {
        setTheme(newTheme.name)
      }

      document.documentElement.classList.remove(currentTheme)
      document.documentElement.classList.add(newTheme.name)
      if (onSwitch) onSwitch()
    },
    [setTheme, currentTheme, onSwitch],
  )

  return (
    <Fragment>
      <List>
        {Object.values(themes).map((theme) => (
          <li
            key={theme.name}
            className={theme.name === currentTheme ? 'active' : ''}
            aria-current={theme.name === currentTheme}
          >
            <button
              onClick={() => {
                switchThemes(theme)
              }}
            >
              <StackIcons>
                <Icon
                  icon="square"
                  css={css`
                    color: ${theme.colors.secondary} !important;
                    filter: drop-shadow(0 0 2px ${theme.colors.accent});
                  `}
                  fixedWidth
                />
                <Icon
                  icon={theme.icon}
                  title={theme.mode}
                  transform="shrink-5"
                  color={theme.colors.onSecondary}
                  fixedWidth
                />
              </StackIcons>
              {theme.displayName}
            </button>
          </li>
        ))}
      </List>
    </Fragment>
  )
}

export { ThemeSwitcher }
export { ThemeSwitcherPopover } from './Popover'
