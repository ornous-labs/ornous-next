import * as React from 'react'

import { render, screen, act } from '@test/utils'
import { post, image } from '@test/fixtures'

import { BlogArticle } from './Article'

describe('blog.Article', () => {
  it('renders correctly', () => {
    const examplePost = {
      ...post,
      description: 'My Article Description',
      content: `
        <h3>Blog Article Header</h3>
        <p>Blog Article Content</p>
      `,
    }

    act(() => {
      render(<BlogArticle post={examplePost} />)
    })

    expect(
      screen.getByRole('heading', {
        level: 1,
        name: 'Post Title',
      }),
    ).toBeInTheDocument()

    expect(
      screen.getByRole('heading', {
        level: 2,
        name: 'Category Name',
      }),
    ).toBeInTheDocument()

    expect(screen.getByText('My Article Description')).toBeInTheDocument()

    expect(
      screen.getByRole('heading', {
        level: 3,
        name: 'Blog Article Header',
      }),
    ).toBeInTheDocument()

    expect(screen.getByText('Blog Article Content')).toBeInTheDocument()

    expect(screen.getByText('1 January 1970')).toBeInTheDocument()

    expect(screen.getByText(/.*reading_time {"count":6}/)).toBeInTheDocument()

    expect(
      screen.getByRole('img', {
        src: 'https://media.graphassets.com/iGPKUaxfQr2WSYtpTW72',
      }),
    ).toBeInTheDocument()
  })

  it('shows message for empty state', () => {
    render(<BlogArticle post={null} />)

    expect(screen.getByText(/missing_post/)).toBeInTheDocument()
  })
})
