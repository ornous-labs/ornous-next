import type { FC } from 'react'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import { css } from '@emotion/react'

import { Breadcrumbs } from './Breadcrumbs'
import { Content } from '../Content'
import { Image } from '../Image'
import { TagList } from '../Tag'
import { formatDate } from '@utils/i18n'
import type { PostBySlugQuery } from '@graphql/generated'
// import { log } from '@utils/logger'

const BlogArticle: FC<Pick<PostBySlugQuery, 'post'>> = ({ post }) => {
  const { locale } = useRouter()
  const { t } = useTranslation('blog')

  if (!post) {
    return <div>{t('missing_post')}</div>
  }

  const width = post.heroImage?.width ?? 0
  const height = post.heroImage?.height ?? 0
  const aspectRatio = width / height

  const formattedDate = formatDate(post.publishedAt, locale)
  const localisedReadingTime = t('reading_time', {
    count: Math.ceil(post.readingTime.minutes!),
  })

  return (
    <article className="prose max-w-none">
      <Breadcrumbs post={post} />
      <h2
        css={css`
          display: inline-block;
          font-family: var(--font-text);
          font-weight: 300;
          font-size: var(--font-text-l);
          letter-spacing: 0.025em;
          border-bottom: 3px solid ${post?.rubrique?.colour.hex};
          margin-bottom: 0.45em;
          text-transform: uppercase;
        `}
        className="mt-5"
      >
        {post?.rubrique?.name}
      </h2>
      <h1 className="text-3xl">{post.title}</h1>
      <span>
        {t('published_on')} <time>{formattedDate}</time>
        &nbsp;&#9679; {localisedReadingTime}
      </span>
      <TagList tags={post.tags} />
      {post.writtenByAi ? (
        <p className="italic">
          {t('disclaimer')}: {t('written_by_ai')}
        </p>
      ) : null}
      <p className="lead">{post.description}</p>
      {!post.heroImage?.url ? (
        'No Image'
      ) : (
        <Image
          alt=""
          src={post.heroImage.url}
          width={800}
          height={800 / aspectRatio}
          placeholder="blur"
          blurDataURL={post?.heroImage?.dataURL}
          sizes="(max-width: 800px) 100vw, 800px"
          css={css`
            width: 100%;
            border-radius: 0.25rem;
            box-shadow: var(--shadow-s);
          `}
          priority
        />
      )}
      <Content content={post.content} />
    </article>
  )
}

export { BlogArticle }
