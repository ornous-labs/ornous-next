import { FC, Fragment } from 'react'
import useTranslation from 'next-translate/useTranslation'

import type { Post, Rubrique } from '@graphql/generated'
import Link from '../Link'
import { useRouter } from 'next/router'

interface Props {
  post?: Pick<Post, 'title' | 'slug'> & {
    rubrique?: Pick<Rubrique, 'name' | 'slug'> | null
  }
  rubrique?: Pick<Rubrique, 'name' | 'slug'> | null
}

export const Breadcrumbs: FC<Props> = ({ post, rubrique }) => {
  const { t } = useTranslation('common')
  const isMobile = false //useIsMobile()
  const { asPath } = useRouter()

  // TODO use router to switch between home and blog based on location
  // if (isMobile) {
  //   if (asPath === '/blog') return null
  //   return (
  //     <div className="flex flex-col gap-1 text-xs capitalize">
  //       <Link href="/blog" icon="arrow-left">
  //         {t('blog:back_to_blog_home')}
  //       </Link>
  //     </div>
  //   )
  // }

  const crumbs: Array<{ name?: string; href: string }> = [
    { name: t('menu_home'), href: '/' },
  ]

  const currentRubrique = rubrique ?? post?.rubrique
  if (currentRubrique) {
    crumbs.push({ name: t('menu_blog'), href: '/blog' })
  }

  if (post) {
    crumbs.push({
      name: currentRubrique?.name,
      href: `/blog/category/${currentRubrique?.slug}`,
    })
  }

  return (
    <div className="flex gap-1 text-xs capitalize">
      {crumbs.map(({ name, href }, index) => {
        const separator =
          index === crumbs.length - 1 ? null : (
            <span className="font-black">&raquo;</span>
          )

        return (
          <Fragment key={name}>
            <Link className="shrink-0" href={href}>
              {name}
            </Link>{' '}
            {separator}
          </Fragment>
        )
      })}
    </div>
  )
}
