import type { FC } from 'react'

import { BlogCard } from '@components/blog/Card'
import { CardGrid, CardWrapper } from '@components/Card'
import type { PostsSlugsQuery } from '@graphql/generated'

export const BlogCardGrid: FC<Pick<PostsSlugsQuery, 'posts'>> = ({ posts }) => {
  return (
    <CardGrid>
      {posts?.map(({ id, ...postData }, index) => (
        <CardWrapper key={id}>
          <BlogCard
            imageLoading={index <= 3 ? 'eager' : 'lazy'}
            {...postData}
          />
        </CardWrapper>
      ))}
    </CardGrid>
  )
}
