import { FC, Fragment } from 'react'
import useTranslation from 'next-translate/useTranslation'

import type {
  PostSlugsForRubriqueBySlugQuery,
  PostsSlugsQuery,
} from '@graphql/generated'
import { Content } from '@components/Layout'
import { Chip } from '@components/Chip'
import Link from '@components/Link'
import { BlogCardGrid } from './Card/Grid'
import { useTheme } from '@emotion/react'
import { desaturate } from 'polished'

interface ListingProps {
  posts: PostsSlugsQuery['posts']
  rubrique?: PostSlugsForRubriqueBySlugQuery['rubrique']
  rubriques?: PostsSlugsQuery['rubriques']
}

const Listing: FC<ListingProps> = ({ posts, rubrique, rubriques }) => {
  const { t } = useTranslation('blog')
  const theme = useTheme()

  return (
    <Content className="fullscreen">
      {rubriques && (
        <Fragment>
          <h1 className="flex justify-center capitalize">{t('blog:posts')}</h1>
          <div className="my-8 flex items-center justify-center gap-4">
            <nav aria-label={t('blog:categories')}>
              <ul className="flex flex-wrap justify-center gap-3">
                <li key="show-all">
                  <Chip colour="#ffffff">
                    <Link
                      href={`/blog/`}
                      aria-current={!rubrique}
                      scroll={false}
                    >
                      {t('show_all')}
                    </Link>
                  </Chip>
                </li>
                {rubriques?.map(({ id, name, slug, colour }) => {
                  const color =
                    theme.mode === 'light'
                      ? colour.hex
                      : desaturate(0.1, colour.hex)
                  return (
                    <li key={id}>
                      <Chip colour={color}>
                        <Link
                          href={`/blog/category/${slug}`}
                          scroll={false}
                          aria-current={
                            rubrique ? slug === rubrique!.slug : false
                          }
                        >
                          {name}
                        </Link>
                      </Chip>
                    </li>
                  )
                })}
              </ul>
            </nav>
          </div>
        </Fragment>
      )}
      <BlogCardGrid posts={posts} />
    </Content>
  )
}

export default Listing
