import { FC, Fragment } from 'react'
import useTranslation from 'next-translate/useTranslation'

import { SideBar } from '@components/Layout'
import { Chip } from '@components/Chip'
import Link from '@components/Link'
import type {
  PostBySlugQuery,
  Rubrique,
  RubriquesSlugsQuery,
} from '@graphql/generated'
import { BlogCard } from './Card'

interface SidebarProps {
  rubrique?: Pick<Rubrique, 'slug'> | null
  rubriques: RubriquesSlugsQuery['rubriques']
  post?: PostBySlugQuery['post']
}

export const BlogSidebar: FC<SidebarProps> = ({
  post,
  rubriques,
  ...props
}) => {
  const { t } = useTranslation()

  const rubrique = post?.rubrique ?? props.rubrique

  return (
    <SideBar>
      {rubriques && (
        <Fragment>
          <h2>{t('blog:categories')}</h2>
          <nav aria-label={t('blog:categories')}>
            <ul>
              {rubriques.map(({ id, name, slug, colour }) => {
                return (
                  <li key={id}>
                    <Chip colour={colour.hex}>
                      <Link
                        href={`/blog/category/${slug}`}
                        aria-current={
                          typeof rubrique !== 'undefined' &&
                          slug === rubrique?.slug
                        }
                      >
                        {name}
                      </Link>
                    </Chip>
                  </li>
                )
              })}
            </ul>
          </nav>
        </Fragment>
      )}
      {post ? (
        <Fragment>
          <h2>{t('blog:related_posts')}</h2>
          <div>
            {post?.rubrique?.relatedPosts.map((postData) => (
              <BlogCard key={postData.id} className="my-3" {...postData} />
            ))}
          </div>
        </Fragment>
      ) : null}
    </SideBar>
  )
}
