import { useState } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import useTranslation from 'next-translate/useTranslation'
import { zodResolver } from '@hookform/resolvers/zod'

import { log } from '@utils/logger'
import { Input, Button, TextArea } from './Field'
import { ContactInputSchema } from '@graphql/generated/validators'
import { ContactInput, useContactMutation } from '@graphql/generated/index'

/* eslint-disable i18next/no-literal-string */
export const ContactForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<ContactInput>({
    resolver: zodResolver(ContactInputSchema()),
  })
  const { t } = useTranslation('contact')
  const [success, setSuccess] = useState(false)
  const [contactMutation] = useContactMutation()

  const onSubmit: SubmitHandler<ContactInput> = async (input) => {
    const { data } = await contactMutation({ variables: { input } })

    if (data?.contact.success) {
      setSuccess(true)
    }
  }

  if (success) {
    return <div>{t('contact_success_message')}</div>
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="mb-4">
      <div className="mt-4">
        <Input
          label={t('name')}
          required
          placeholder="Jane Doe"
          error={errors?.name}
          {...register('name')}
        />
      </div>

      <div className="mt-4">
        <Input
          type={t('email')}
          label="Email"
          placeholder="jane.doe@incognito.corp"
          error={errors?.email}
          {...register('email')}
        />
      </div>

      <div className="mt-4">
        <TextArea
          label={t('message')}
          placeholder="Hi Ousmane, I am Jane from Incognito Corp. I wanted to
          get in touch to..."
          error={errors?.message}
          rows={8}
          required
          {...register('message')}
        />
      </div>

      <div className="my-4">
        <Button type="submit" disabled={isSubmitting}>
          {t('send')}
        </Button>
      </div>
    </form>
  )
}
