import type { FC } from 'react'
import useTranslation from 'next-translate/useTranslation'
import type { FieldError } from 'react-hook-form'

interface ErrorProps {
  id: string
  error: FieldError
}

export const Error: FC<ErrorProps> = ({ id, error }) => {
  const { t } = useTranslation('form')

  return (
    <span
      id={id}
      role="alert"
      aria-live="assertive"
      className="text-sm text-error"
    >
      {t('errors.' + String(error.message))}
    </span>
  )
}
