import React from 'react'

import { render, screen } from '@test/utils'

import { Button } from './Button'

describe('form.Button', () => {
  it('renders correctly', () => {
    render(<Button type="submit">Submit</Button>)

    expect(screen.getByRole('button', { name: 'Submit' })).toBeInTheDocument()
  })
})
