import type { ButtonHTMLAttributes, ReactNode } from 'react'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children: string
  Icon?: ReactNode
}

export const Button = ({ children, Icon, ...props }: ButtonProps) => {
  return (
    <button
      className="focus:shadow-outline flex gap-1 rounded bg-secondary-200 py-2 px-4 font-bold capitalize text-on-secondary hover:bg-secondary-200/80 focus:outline-none focus:ring-secondary"
      {...props}
    >
      {children}
      {Icon}
    </button>
  )
}
