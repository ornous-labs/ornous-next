import * as React from 'react'

import { render, screen, cleanup } from '@test/utils'

import { Input } from './'

describe('form.Input', () => {
  afterEach(() => {
    cleanup()
  })

  it('renders correctly', () => {
    render(<Input name="form-input" label="my input" />)

    const input = screen.getByRole('textbox', { name: 'form-input' })

    expect(input).toBeInTheDocument()
    expect(screen.queryByRole('alert')).not.toBeInTheDocument()
    expect(screen.getByRole('textbox', { name: 'form-input' })).toHaveAttribute(
      'aria-invalid',
      'false',
    )
    expect(input).not.toHaveAccessibleErrorMessage()
  })

  it('renders errors correctly', () => {
    const error = {
      type: 'required',
      message: 'required',
    }

    render(<Input name="form-input" label="my input" error={error} />)

    const input = screen.getByRole('textbox', { name: 'form-input' })

    expect(screen.queryByRole('alert')).toBeInTheDocument()
    expect(input).toHaveAttribute('aria-invalid', 'true')
    expect(input).toHaveAccessibleErrorMessage('errors.required')
  })
})
