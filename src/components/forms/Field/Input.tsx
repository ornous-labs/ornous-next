import React, { Fragment } from 'react'
import type { InputHTMLAttributes } from 'react'
import type { FieldError } from 'react-hook-form'

import { Label } from '../Label'
import { Error } from '../Error'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string
  error?: FieldError
}

export const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ required, ...props }, ref) => {
    const { error, label, name } = props

    const errorId = `${name}-error`

    return (
      <Fragment>
        <Label htmlFor={name} required={required}>
          {label}
        </Label>
        <input
          {...props}
          name={name}
          id={name}
          ref={ref}
          aria-required={required}
          aria-label={name}
          aria-invalid={error ? true : false}
          aria-errormessage={errorId}
          className={`focus:shadow-outline form-input w-full appearance-none rounded border py-2 px-3 leading-tight text-gray-700 shadow focus:border-accent focus:outline-none focus:ring-accent ${
            error ? 'border-2 border-error' : ''
          }`}
        />
        {error && <Error id={errorId} error={error} />}
      </Fragment>
    )
  },
)

Input.displayName = 'Input'
