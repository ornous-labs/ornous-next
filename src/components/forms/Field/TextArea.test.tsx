import * as React from 'react'

import { act, render, screen, cleanup } from '@test/utils'

import { TextArea } from './'

describe('form.TextArea', () => {
  afterEach(cleanup)

  it('renders correctly', () => {
    act(() => {
      render(<TextArea name="form-input" label="my input" />)
    })

    expect(screen.getByLabelText('form-input')).toBeInTheDocument()
    expect(screen.queryByRole('alert')).not.toBeInTheDocument()
    expect(screen.getByRole('textbox', { name: 'form-input' })).toHaveAttribute(
      'aria-invalid',
      'false',
    )
  })

  it('renders errors correctly', () => {
    const error = {
      type: 'required',
      message: 'required',
    }
    render(<TextArea name="form-input" label="my input" error={error} />)

    const input = screen.getByRole('textbox', { name: 'form-input' })

    expect(screen.queryByRole('alert')).toBeInTheDocument()
    expect(input).toHaveAttribute('aria-invalid', 'true')
    expect(input).toHaveAccessibleErrorMessage('errors.required')
  })
})
