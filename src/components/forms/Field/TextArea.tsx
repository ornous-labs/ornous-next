import React, { type TextareaHTMLAttributes } from 'react'

import { Label } from '../Label'
import { Error } from '../Error'
import type { FieldError } from 'react-hook-form'

interface TextAreaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  name: string
  label: string
  error?: FieldError
}

export const TextArea = React.forwardRef<HTMLTextAreaElement, TextAreaProps>(
  ({ required, ...props }, ref) => {
    const { error, label, name } = props

    return (
      <div className="mt-4">
        <Label htmlFor={name} required={required}>
          {label}
        </Label>
        <textarea
          ref={ref}
          id={name}
          aria-required={required}
          aria-label={name}
          aria-invalid={error ? true : false}
          aria-errormessage={`${name}-error`}
          className={`focus:shadow-outline form-textarea w-full appearance-none rounded border py-2 px-3 leading-tight text-gray-700 shadow focus:border-primary/60 focus:outline-none focus:ring-primary/60 ${
            error ? 'border-2 border-error' : ''
          }`}
          {...props}
        />
        {error && <Error id={`${name}-error`} error={error} />}
      </div>
    )
  },
)

TextArea.displayName = 'TextArea'
