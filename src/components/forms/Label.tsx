import useTranslation from 'next-translate/useTranslation'
import type { FC, LabelHTMLAttributes } from 'react'

interface Props extends LabelHTMLAttributes<HTMLLabelElement> {
  required?: boolean
}

export const Label: FC<Props> = ({ htmlFor, required = false, children }) => {
  const { t } = useTranslation('form')

  return (
    <label htmlFor={htmlFor} className="mb-2 block text-sm font-bold">
      {children}
      {required && <span className="text-sm"> ({t('required')})</span>}
    </label>
  )
}
