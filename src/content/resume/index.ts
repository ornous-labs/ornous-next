import skills from './skills'
import history from './history'

const data = {
  title: 'Software & Platform Engineer',
  statement:
    'Software engineer focused on putting the right software in the hand of users through collaboration, learning and experimentation. ' +
    'I am most comfortable delivering resilient SPAs and microservices safely and reliably using continuous delivery and infrastructure as ' +
    'code where my experience working at different levels across a range of technical stacks allows me to quickly identify opportunities ' +
    'and drive improvements according to business needs.',
  skills,
  history,
  education: [
    {
      school: 'IP Formation',
      subject: 'Web Development',
      graduation: 2007,
      location: 'Paris, France',
    },
    {
      school: 'Paris 13',
      subject: 'Electrical/Electronics Engineering',
      graduation: 2006,
      location: 'Paris, France',
    },
  ],
  certifications: ['Zend Certified PHP Engineer', 'Certified MySQL Developer'],
}

export default data
