/*
 * AliCloud, GCP, AWS, Cloudflare
 * Podman (Buildah)
 * Crossplane
 *
 * Redis/Memcached
 *
 * RabbitMQ, Kafka
 *
 * github workflows/actions
 *
 * Open Policy Agent
 *
 * Disaster Recovery
 */
const data = [
  {
    heading: 'Methodologies',
    entries: [
      'Agile',
      'Lean',
      'Kanban',
      'DevOps',
      'GitOps',
      'DDD',
      'Chaos Engineering',
    ],
  },
  {
    heading: 'Cloud',
    entries: ['AWS', 'GCP', 'AliCloud'],
  },
  {
    heading: 'Languages',
    entries: ['TypeScript', 'JavaScript', 'Bash', 'HTML/CSS', 'PHP'],
  },
  {
    heading: 'Persistence',
    entries: ['MySQL', 'Postgres', 'Mongo', 'CockroachDB', 'Redis', 'S3'],
  },
  {
    heading: 'Frameworks/Tools',
    entries: [
      'React',
      'Redux',
      'RxJS',
      'NextJS',
      'Gatsby',
      'Tailwind CSS',
      'Styled Componenents',
      'Symfony',
      'Laravel',
      'Storybook',
    ],
  },
  {
    heading: 'Security',
    entries: ['Vault', 'Trivy', 'OAuth', 'Keycloak', 'OWASP ZAP', 'SAST'],
  },
  {
    heading: 'Infrastructure/Configuration as Code',
    entries: ['Terraform', 'Ansible', 'Docker', 'Helm'],
  },
  {
    heading: 'API/IPC',
    entries: ['Rest', 'GraphQL', 'gRPC', 'tRPC', 'Kafka', 'RabbitMQ'],
  },
  {
    heading: 'Testing',
    entries: [
      'Cypress',
      'Selenium',
      'Jest',
      'Mutation Testing',
      'Mountebank',
      'artillery',
      'k6',
      'Consumer Driven Contracts',
      'Performance Testing',
    ],
  },
  {
    heading: 'Telemetry',
    entries: [
      'Prometheus',
      'Grafana',
      'Jaeger',
      'Zipkin',
      'Loki',
      'Tempo',
      'OTEL',
      'Thanos',
      'ELK',
    ],
  },
  {
    heading: 'Kubernetes',
    entries: [
      'Kong',
      'LinkerD',
      'Istio',
      'Kind',
      'OPA',
      'Predator',
      'Datree',
      'Skaffold',
      'Tilt',
    ],
  },
  {
    heading: 'CI/CD',
    entries: [
      'Gitlab CI',
      'Jenkins',
      'Circle CI',
      'Flagger',
      'Argo Rollouts',
      'Progressive Delivery',
    ],
  },
]

export default data
