const socialLinks = [
  {
    id: '1',
    name: 'LinkedIn',
    url: 'https://www.linkedin.com/in/ornous/',
    title: 'More about me on LinkedIn',
    brandIcon: 'linkedin',
  },
  {
    id: '2',
    name: 'Twitter',
    url: 'https://twitter.com/@ornous',
    title: 'Follow me on Twitter',
    brandIcon: 'twitter',
  },
  {
    id: '3',
    name: 'GitLab',
    url: 'https://gitlab.com/ornous',
    title: 'Checkout my GitLab projects',
    brandIcon: 'gitlab',
  },
  {
    id: '4',
    name: 'GitHub',
    url: 'https://github.com/ornous',
    title: 'My GitHub Profile',
    brandIcon: 'github',
  },
  {
    id: '5',
    name: 'GoodReads',
    url: 'https://goodreads.com/ornous',
    title: "See what I'm reading",
    brandIcon: 'goodreads',
  },
]

export default socialLinks
