import { PrismaClient } from '@prisma/client'
import type { Logger } from 'next-axiom'

import { log } from '@utils/logger'

export interface Context {
  logger: Logger
  prisma?: PrismaClient
}

const context: Context = {
  logger: log,
}

if (typeof window === 'undefined') {
  context.prisma = new PrismaClient()
}

export { context }
