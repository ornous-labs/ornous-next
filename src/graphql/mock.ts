import casual from 'casual'
import { addMocksToSchema } from '@graphql-tools/mock'
import { log } from '@utils/logger'

import type { GraphQLSchema } from 'graphql'

// TODO Load these asynchronously
import img1 from '../assets/mocks/image1.jpg'
import img2 from '../assets/mocks/image2.png'
import img3 from '../assets/mocks/image3.jpg'
import img4 from '../assets/mocks/image4.jpg'
import img5 from '../assets/mocks/image5.jpg'
import img6 from '../assets/mocks/image6.jpg'
import img7 from '../assets/mocks/image7.jpg'
import img8 from '../assets/mocks/image8.jpg'
import img9 from '../assets/mocks/image9.jpg'
import img10 from '../assets/mocks/image10.webp'
import img11 from '../assets/mocks/image11.jpg'
import img12 from '../assets/mocks/image12.jpg'
import img13 from '../assets/mocks/image13.jpg'
import type { Post, Rubrique } from './generated'

// const store = createMockStore({ schema: executableSchema })
//

const loadRandomImage = () => {
  const images = [
    img1,
    img2,
    img3,
    img4,
    img5,
    img6,
    img7,
    img8,
    img9,
    img10,
    img11,
    img12,
    img13,
  ]

  return casual.random_element(images)
}

const mockRubriques = [
  { name: 'Web Development', slug: 'web-development', posts: [{}, {}, {}] },
  { name: 'Architecture', slug: 'architecture' },
  { name: 'DevOps & SRE', slug: 'devops-and-sre' },
]

export const mockSchema = (schema: GraphQLSchema) => {
  return addMocksToSchema({
    // store,
    schema,
    mocks: {
      DateTime: () => casual.date(),
      Hex: () => casual.rgb_hex,
      Asset: () => {
        const img = loadRandomImage()
        return {
          url: `http://localhost:3000${img.src}`,
          width: img.width,
          height: img.height,
          dataURL: img.blurDataURL,
        }
      },
      Post: () => {
        const title = casual.short_description

        return {
          slug: title.replaceAll(' ', '-'),
          publishedAt: casual.date(),
          title,
          description: casual.description,
          content: `
          <h2>${casual.title}</h2>
          <p>${casual.sentences(10)}</p>

          <h2>${casual.title}</h2>
          <p>${casual.sentences(5)}</p>
          <p>${casual.sentences(10)}</p>

          <h2>${casual.title}</h2>
          <p>${casual.sentences(8)}</p>
          <p>${casual.sentences(2)}</p>

          <h2>${casual.title}</h2>
          <p>The brown dog jumped over the lazy fox.</p>
          <p>Is this piece of text easy enough to read? CSS Container queries
          are finally here, and that means our responsive layouts are about to
          get better. We've been building responsive websites with media queries
          for a while now and that's about to change.</p>
          `,
          readingTime: {
            minutes: casual.month_number,
          },
          tags: [
            { title: 'nextjs', slug: 'next' },
            { title: 'kubernetes', slug: 'k8s' },
          ],
        }
      },
      Query: () => ({}),
    },
    resolvers: {
      Query: {
        posts: () => {
          const nbArticles = casual.integer(5, 12)
          return new Array(nbArticles).fill({}, 0, nbArticles)
        },
        rubriques: () => mockRubriques,
        rubrique: (
          _: Rubrique,
          args: { where: { slug: string }; first: number },
        ) => {
          const { slug } = args.where

          log.info('rubrique details was called', { args })
          const rubrique = mockRubriques.find(
            (rubrique) => rubrique.slug === slug,
          )

          // const nbArticles = args.first ?? casual.integer(3, 10)
          // const posts = new Array(nbArticles).fill({}, 0, nbArticles)
          return {
            ...rubrique,
            title: casual.sentence,
            description: casual.description,
          }
        },
        post: (_: Post, args: { where: { slug: string } }) => {
          log.info('post detail page was called', { args })

          return {
            __typename: 'Post',
            id: casual.uuid,
            title: args?.where?.slug?.replaceAll('-', ' '),
            rubrique: casual.random_element([
              { name: 'Web Development', slug: 'web-development' },
              { name: 'Architecture', slug: 'architecture' },
              { name: 'DevOps & SRE', slug: 'devops-and-sre' },
            ]),
          }
        },

        page: () => {
          log.info('page detail was called')

          return {
            __typename: 'Page',
            id: casual.uuid,
          }
        },
      },
    },
    preserveResolvers: false,
  })
}
