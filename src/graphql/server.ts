import { ApolloServer } from 'apollo-server-micro'
import {
  // ApolloServerPluginLandingPageLocalDefault,
  ApolloServerPluginLandingPageProductionDefault,
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginCacheControl,
} from 'apollo-server-core'

import { log, ApolloRequestLogger } from '../utils/logger'

import { context } from './context'
import { createExecutableSchema } from './schema'

export const createApolloServer = async () => {
  return new ApolloServer({
    context,
    logger: log,
    schema: await createExecutableSchema(),
    cache: 'bounded',
    csrfPrevention: true,
    dataSources: () => ({}),
    plugins: [
      process.env.NODE_ENV === 'production'
        ? ApolloServerPluginLandingPageProductionDefault({
            graphRef: 'Ornous@current',
            footer: false,
          })
        : ApolloServerPluginLandingPageGraphQLPlayground,
      ApolloServerPluginCacheControl({ defaultMaxAge: 5 }),
      ApolloRequestLogger,
    ],
  })
}
