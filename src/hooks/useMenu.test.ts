import { renderHook, act } from '@test/utils'

// import { MouseEventHandler } from 'react'

import useMenu from './useMenu'

describe('hooks.useMenu', () => {
  const testEvent = {
    preventDefault: jest.fn(),
  }

  it('menu is closed by default', () => {
    const harness = renderHook(() => useMenu())
    expect(harness.result.current.isOpen).toBe(false)
  })

  it('opens and closes menu', () => {
    const harness = renderHook(() => useMenu())

    act(() => {
      harness.result.current.openMenu(testEvent)
    })
    expect(harness.result.current.isOpen).toBe(true)

    act(() => {
      harness.result.current.closeMenu(testEvent)
    })
    expect(harness.result.current.isOpen).toBe(false)
  })

  it('Toggles open state', () => {
    const harness = renderHook(() => useMenu())

    act(() => {
      harness.result.current.toggle(testEvent)
    })
    expect(harness.result.current.isOpen).toBe(true)

    act(() => {
      harness.result.current.toggle(testEvent)
    })
    expect(harness.result.current.isOpen).toBe(false)
  })
})
