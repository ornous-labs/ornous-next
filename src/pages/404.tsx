import useTranslation from 'next-translate/useTranslation'
import Error from 'next/error'

export default function NotFound() {
  const { t } = useTranslation('common')
  // do not record an exception in Sentry for 404
  return <Error statusCode={404} title={t('page_not_found')} />
}
