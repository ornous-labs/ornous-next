import { Fragment, type FC } from 'react'
import type {
  GetServerSideProps,
  GetStaticPaths,
  GetStaticPathsResult,
} from 'next'
import Image from 'next/image'
import useTranslation from 'next-translate/useTranslation'
import type { ParsedUrlQuery } from 'querystring'

import type { LandingBySlugQuery } from '@graphql/generated'
import { LandingBySlugDocument, LandingSlugsDocument } from '@graphql/generated'
import { query } from '@graphql/query'
import { log } from '@utils/logger'
import SEO from '@components/SEO'
import { Content as HtmlContent } from '@components/Content'
import { Content } from '@components/Layout'

export const LandingPage: FC<{ data: LandingBySlugQuery }> = ({ data }) => {
  const { t } = useTranslation('common')
  const page = data?.page

  if (!page) {
    return <div>{t('loading')}</div>
  }

  const width = page.heroImage[0]?.width ?? 0
  const height = page.heroImage[0]?.height ?? 0
  const aspectRatio = width / height

  return (
    <Fragment>
      <SEO
        title={page.title}
        description={page.description}
        image={page.heroImage[0].url}
      />

      <Content>
        <article className="prose max-w-none">
          <h1>{page.title}</h1>
          <p>{page.description}</p>
          <Image
            alt=""
            src={page.heroImage[0].url}
            width={800}
            height={800 / aspectRatio}
            placeholder="blur"
            blurDataURL={page?.heroImage[0]?.dataURL}
            priority
          />
          <HtmlContent content={page.content} />
        </article>
      </Content>
    </Fragment>
  )
}

export const getStaticProps: GetServerSideProps = async ({
  params,
  locale,
  defaultLocale,
}) => {
  const variables = {
    locales: [
      ['es', 'it'].includes(locale as string) ? 'en' : locale,
      defaultLocale,
    ].filter((locale) => locale),
    slug: params?.slug,
  }

  const { data, errors } = await query(LandingBySlugDocument, variables)

  if (errors) {
    throw new Error(`Error retrieving landing page data ${errors[0]}`)
  }

  return {
    props: { data },
    revalidate: 60000,
  }
}

export const getStaticPaths: GetStaticPaths<ParsedUrlQuery> = async ({
  locales,
}) => {
  const paths: GetStaticPathsResult['paths'] = []

  const { data, errors } = await query(LandingSlugsDocument)

  if (errors) {
    log.error('Error retrieving landing page static paths', { errors })

    throw new Error('Error retrieving landing page static paths')
  }

  data?.pages?.forEach(({ slug }: { slug: string }) => {
    locales?.forEach((locale: string) => {
      paths.push({ params: { slug }, locale })
    })
  })

  console.log({ paths })

  return { paths, fallback: false }
}

export default LandingPage
