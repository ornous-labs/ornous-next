import { Fragment } from 'react'
import Document, {
  type DocumentContext,
  type DocumentInitialProps,
  Html,
  Head,
  Main,
  NextScript,
} from 'next/document'
import Script from 'next/script'
// import containerQueryPolyfill from 'container-query-polyfill'

import themeSelector from '../ThemeSelector'
import { renderStatic } from '@utils/style'

class CustomDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext,
  ): Promise<DocumentInitialProps> {
    const page = await ctx.renderPage()
    const { css, ids } = await renderStatic(page.html)
    const initialProps = await Document.getInitialProps(ctx)

    return {
      ...initialProps,
      styles: (
        <Fragment>
          {initialProps.styles}
          <style
            data-emotion={`css ${ids.join(' ')}`}
            dangerouslySetInnerHTML={{ __html: css }}
          />
        </Fragment>
      ),
    }
  }

  render() {
    return (
      <Html>
        <Head>
          {/* <link href="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js" /> */}
          {/* <link href="/container-query-polyfill.modern.js" /> */}
          {/* <link href="/mermaid.min.js" /> */}
        </Head>
        <body>
          <Script
            id="theme-selector"
            strategy="beforeInteractive"
            dangerouslySetInnerHTML={{ __html: themeSelector }}
          />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default CustomDocument
