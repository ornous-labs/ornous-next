import type { NextApiRequest, NextApiResponse } from 'next'

import { generateFeedFiles } from '@utils/SEO'
import { log } from '../../utils/logger'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.query.secret !== process.env.SEO_GEN_TOKEN) {
    return res.status(401).json({ message: 'Invalid token' })
  }

  try {
    await generateFeedFiles()

    return res.status(200).json({ status: 'success' })
  } catch (err) {
    log.error('Feed Generation failed', { err })
    res.status(500).json({ message: 'Failed to generate feed' })
    res.end()
    return
  }
}

export const config = {
  api: {
    externalResolver: true,
  },
}

export default handler
