import type { NextApiRequest, NextApiResponse } from 'next'

import { generateSitemap } from '@utils/SEO'

import { log } from '../../utils/logger'

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse<{ message?: string }>,
) => {
  if (req.query.secret !== process.env.SEO_GEN_TOKEN) {
    return res.status(401).json({ message: 'Invalid token' })
  }

  try {
    await generateSitemap()

    return res.status(200).end()
  } catch (err) {
    log.error('Sitemap Generation failed', { err })
    res.status(500).json({ message: 'Failed to generate sitemap' })
    res.end()
    return
  }
}

export const config = {
  api: {
    externalResolver: true,
  },
}

export default handler
