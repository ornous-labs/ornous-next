import type { NextApiHandler /* NextApiRequest */ } from 'next'
import microCors from 'micro-cors'

import { createApolloServer } from '@graphql/server'
import type { RequestHandler } from 'micro'

const cors = microCors({})
const handler: NextApiHandler = async (req, res) => {
  const apolloServer = await createApolloServer()
  await apolloServer.start()
  const handler = await apolloServer.createHandler({ path: '/api/graphql' })(
    req,
    res,
  )

  return handler
}

export const config = {
  api: {
    externalResolver: true,
    bodyParser: false,
  },
}

export default cors(handler as RequestHandler)
