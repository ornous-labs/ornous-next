import type { NextApiRequest, NextApiResponse } from 'next'
import { log } from '../../utils/logger'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.query.secret !== process.env.ISR_REVALIDATE_TOKEN) {
    return res.status(401).json({ message: 'Invalid token' })
  }

  if (!req.query.path) {
    return res.status(400).json({ message: 'Missing path to revalidate' })
  }

  if (typeof req.query.path !== 'string') {
    return res.status(400).json({
      message: 'Can only revalidate one path at a time',
    })
  }

  try {
    await res.revalidate(req.query.path)

    return res.status(200).json({ revalidated: true })
  } catch (err) {
    log.error('Revalidation failed', { err })
    res.status(500).json({ message: 'Error revalidating' })
    res.end()
    return
  }
}

export const config = {
  api: {
    externalResolver: true,
  },
}

export default handler
