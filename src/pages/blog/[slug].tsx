import { Fragment, type FC } from 'react'
import type {
  GetServerSideProps,
  GetStaticPaths,
  GetStaticPathsResult,
} from 'next'
import { useTheme } from '@emotion/react'
import Head from 'next/head'
import useTranslation from 'next-translate/useTranslation'

import { Content } from '@components/Layout'
import { BlogArticle } from '@components/blog/Article'
import {
  PostsSlugsDocument,
  PostBySlugDocument,
  type PostBySlugQuery,
} from '@graphql/generated'
import { query } from '@graphql/query'
import { log } from '@utils/logger'
import SEO from '@components/SEO'
import { BlogSidebar } from '@components/blog/Sidebar'
import { BlogCardGrid } from '@components/blog/Card/Grid'
import type { ParsedUrlQuery } from 'querystring'

export const BlogArticlePage: FC<{ data: PostBySlugQuery }> = ({ data }) => {
  const { t } = useTranslation('blog')
  const { hljsTheme } = useTheme()

  const post = data?.post

  // TODO revisit logic after client side hydrated queries
  if (!post) {
    return <div>{t('loading')}</div>
  }

  return (
    <Fragment>
      <SEO
        type="article"
        title={post.title}
        description={post.description}
        image={post.heroImage?.url}
      />
      <Head>
        {/* TODO Move into head script with enable/disable triggered here */}
        {/* TODO or, install highlight.js and load styles from theme */}

        <link rel="stylesheet" href={hljsTheme} key="hljs" />
      </Head>
      <Content className="fullscreen">
        <BlogArticle post={post} />
        <BlogSidebar
          rubriques={data?.rubriques}
          rubrique={data?.post?.rubrique}
        />
      </Content>
      <Content>
        <h2 className="text-center text-xl">{t('blog:related_posts')}</h2>
        <BlogCardGrid posts={data?.post?.rubrique?.relatedPosts} />
      </Content>
    </Fragment>
  )
}

export const getStaticProps: GetServerSideProps = async ({
  params,
  locale,
  defaultLocale,
}) => {
  const variables = {
    slug: params?.slug as string,
    locales: [
      ['es', 'it'].includes(locale as string) ? 'en' : locale,
      defaultLocale,
    ],
  }

  const { data, errors } = await query(PostBySlugDocument, variables)

  if (errors) {
    log.error('Error loading post data', { errors })

    // TODO reconsider using 404 for this
    return { notFound: true }
  }

  if (!data?.post) {
    log.error('Post not found')
    return { notFound: true }
  }

  return {
    props: { data },
    revalidate: 5000,
  }
}

export const getStaticPaths: GetStaticPaths<ParsedUrlQuery> = async ({
  locales,
}) => {
  const paths: GetStaticPathsResult['paths'] = []

  const variables = {
    locales: ['en', 'fr'],
    includeCurrent: true,
  }

  const { data, errors } = await query(PostsSlugsDocument, variables)

  if (errors) {
    log.error('Failed to retrieve blog static paths', { errors })

    throw new Error('Failed to retrieve blog static paths')
  }

  data?.posts?.forEach(({ slug }: { slug: string }) => {
    locales?.forEach((locale: string) => {
      paths.push({ params: { slug }, locale })
    })
  })

  return { paths, fallback: false }
}

export default BlogArticlePage
