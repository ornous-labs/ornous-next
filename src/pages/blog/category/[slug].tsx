import { Fragment, FC } from 'react'
import type {
  GetServerSideProps,
  GetStaticPaths,
  GetStaticPathsResult,
} from 'next'
import useTranslation from 'next-translate/useTranslation'
import type { ParsedUrlQuery } from 'querystring'

import {
  PostsForRubriqueDocument,
  RubriquesSlugsDocument,
  type PostsForRubriqueQuery,
  type Rubrique,
} from '@graphql/generated'
import SEO from '@components/SEO'
import { log } from '@utils/logger'
import { query } from '@graphql/query'
import Listing from '@components/blog/Listing'

const BlogCategoryList: FC<{ data: PostsForRubriqueQuery }> = ({ data }) => {
  const { t } = useTranslation('common')
  const rubrique = data?.rubrique as Rubrique

  if (!rubrique) {
    return <div>{t('loading')}</div>
  }

  return (
    <Fragment>
      <SEO
        title={`${rubrique.name}: ${rubrique.title}`}
        description={rubrique.description}
      />

      <Listing
        rubrique={rubrique}
        posts={rubrique?.posts}
        rubriques={data?.rubriques}
      />
    </Fragment>
  )
}

export const getStaticProps: GetServerSideProps = async ({
  params,
  locale,
  defaultLocale,
}) => {
  if (typeof params?.slug !== 'string') {
    throw new Error('Slug should be a string')
  }

  const variables = {
    slug: params?.slug,
    locales: [
      ['es', 'it'].includes(locale as string) ? 'en' : locale,
      defaultLocale,
    ],
  }

  const { data, errors } = await query(PostsForRubriqueDocument, variables)

  if (errors) {
    log.error('Failed to retrieve blog category static page', { errors })

    throw new Error('Failed to retrieve blog category page')
  }

  if (!data?.rubrique) {
    return { notFound: true }
  }

  return { props: { data }, revalidate: 10000 }
}

export const getStaticPaths: GetStaticPaths<ParsedUrlQuery> = async ({
  locales,
}) => {
  const paths: GetStaticPathsResult['paths'] = []

  const variables = {
    locales: ['en', 'fr'],
    includeCurrent: true,
  }

  const { data, errors } = await query(RubriquesSlugsDocument, variables)

  if (errors) {
    log.error('Failed to retrieve blog category static paths', { errors })
    throw new Error('Failed to retrieve blog category static paths')
  }

  data?.rubriques.forEach(({ slug }: { slug: string }) => {
    locales?.forEach((locale: string) => {
      paths.push({ params: { slug }, locale })
    })
  })

  return { paths, fallback: 'blocking' }
}

export default BlogCategoryList

export { BlogCategoryList }
