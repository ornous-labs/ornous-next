import { Fragment, type FC } from 'react'
import type { GetServerSideProps } from 'next'
import useTranslation from 'next-translate/useTranslation'

import { log } from '@utils/logger'
import SEO from '@components/SEO'
import { query } from '@graphql/query'
import { PostsSlugsDocument, type PostsSlugsQuery } from '@graphql/generated'
import { generateFeedFiles, generateSitemap } from '@utils/SEO'
import Listing from '@components/blog/Listing'

export const BlogHome: FC<{ data: PostsSlugsQuery }> = ({ data }) => {
  const { t } = useTranslation('blog')

  return (
    <Fragment>
      <SEO title={t('blog:title')} />

      <Listing posts={data?.posts} rubriques={data?.rubriques} />
    </Fragment>
  )
}

export const getStaticProps: GetServerSideProps = async ({
  locale,
  defaultLocale,
}) => {
  const variables = {
    locales: new Set([
      ['es', 'it'].includes(locale as string) ? 'en' : locale,
      defaultLocale,
    ]),
    includeCurrent: false,
  }

  const { data, errors } = await query(PostsSlugsDocument, variables)

  await generateFeedFiles()
  await generateSitemap()

  if (errors) {
    log.error('Error loading posts', { errors })

    // TODO reconsider using 404 for this
    return { notFound: true }
  }

  return {
    props: { data },
    revalidate: 300,
  }
}

export default BlogHome
