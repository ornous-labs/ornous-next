import type { FC } from 'react'

import { ContactForm } from '@components/forms/ContactForm'
import useTranslation from 'next-translate/useTranslation'
import SEO from '@components/SEO'
import { Content } from '@components/Layout'

const Contact: FC = () => {
  const { t } = useTranslation('contact')
  return (
    <Content>
      <SEO title={t('title')} />
      <h1>{t('title')}</h1>

      <ContactForm />
    </Content>
  )
}

export default Contact
