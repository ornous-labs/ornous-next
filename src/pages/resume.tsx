import type { NextPage } from 'next'
import styled from '@emotion/styled'
import { css } from '@emotion/react'
import useTranslation from 'next-translate/useTranslation'

import data from '../content/resume'
import SEO from '@components/SEO'
import { Content } from '@components/Layout'
import { FC, useState } from 'react'

const Doc = styled.article`
  font-size: var(--font-text-s);
  font-family: var(--font-text);

  p {
    font-size: var(--font-text-m);
  }

  h1,
  h2 {
    text-align: center;
    font-weight: 500;
    font-family: var(--font-heading);
  }

  h1 {
    text-decoration: underline;
    font-weight: 800;
    line-height: 1.1111;
    margin: 0 0.8889em;
  }

  h2 {
    font-weight: 700;
    margin-block: 2em 1em;
    line-height: 1.3333;
  }

  h1 + p {
    line-height: 1.75;
    margin-top: 1.25em;
    margin-bottom: 1.25em;
  }
`

const positionStyles = css`
  font-size: var(--font-text-s);

  h3 {
    font-size: var(--font-text-m);
    font-weight: 400;
  }

  h4 {
    font-family: var(--font-heading);
    font-size: var(--font-text-m);
    font-weight: 600;
  }

  div > div {
    font-size: var(--font-text-s);
  }
`

const SeeMore = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false)

  const label = isOpen ? 'Show Less' : 'Show More'

  return (
    <>
      <button onClick={() => setIsOpen(!isOpen)}>{label}</button>
      <div
        css={css`
          display: grid;
          grid-template-rows: 0fr;

          // var(--transition-slow);
          transition: grid-template-rows 500ms;


          &[aria-expanded='true'] {
            grid-template-rows: 1fr;
          }

          div {
            overflow: hidden;
          }
        `}
        aria-expanded={isOpen}
      >
        <div>{children}</div>
      </div>
    </>
  )
}

/* eslint-disable i18next/no-literal-string */
const Resume: NextPage = () => {
  const { t } = useTranslation('resume')

  return (
    <Content>
      <Doc>
        <SEO title={`${data.title} ${t('common:menu_resume')}`} />

        <h1 className="mb-2">{data.title}</h1>
        <p className="my-5">{data.statement}</p>

        <section>
          <h2 className="mt-2 mb-1 text-xl capitalize">{t('skills')}</h2>
          {data.skills.map(({ heading, entries }) => (
            <p key={heading}>
              <span className="font-heading font-semibold">{heading}: </span>
              {entries.join(', ')}
            </p>
          ))}
        </section>

        <section>
          <h2 className="mt-2 mb-1 capitalize">{t('job_history')}</h2>
          <ul>
            {data.history.map((job) => (
              <li
                key={job.employer.name + job.title}
                className="mt-6 mb-8"
                css={positionStyles}
              >
                <div className="flex justify-between">
                  <div>
                    <h4>{job.employer.name}</h4>
                    <h3>{job.title}</h3>
                  </div>
                  <div>
                    <div>{job.employer.location}</div>
                    <div>
                      {job.from} - {job.to}
                    </div>
                  </div>
                </div>
                <List className="mt-1 pl-3">
                  {job.achievements.map((achievement) => (
                    <li key={achievement}>{achievement}</li>
                  ))}
                </List>
                <span>Key Skills: {job?.keySkills?.join(', ')}</span>
              </li>
            ))}
          </ul>
        </section>

        <section>
          <h2 className="mt-2 mb-1 text-xl capitalize">{t('education')}</h2>
          <ul>
            {data.education.map(({ school, subject, graduation, location }) => (
              <li key={school} className="my-2">
                <div className="flex justify-between">
                  <span>
                    {school} - {subject}
                  </span>
                  <span>
                    {location} - {graduation}
                  </span>
                </div>
              </li>
            ))}
          </ul>
        </section>
      </Doc>
    </Content>
  )
}

const List = styled.ul`
  li {
    list-style-type: disc;
  }
`

export default Resume
