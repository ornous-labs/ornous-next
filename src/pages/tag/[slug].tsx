import { Fragment, FC } from 'react'
import type {
  GetServerSideProps,
  GetStaticPaths,
  GetStaticPathsResult,
} from 'next'
import useTranslation from 'next-translate/useTranslation'
import type { ParsedUrlQuery } from 'querystring'

import {
  PostsForTagDocument,
  TagsSlugsDocument,
  type PostsForTagQuery,
  type Tag,
} from '@graphql/generated'
import SEO from '@components/SEO'
import { log } from '@utils/logger'
import { query } from '@graphql/query'
import Listing from '@components/blog/Listing'

const BlogTagList: FC<{ data: PostsForTagQuery }> = ({ data }) => {
  const { t } = useTranslation('common')
  const tag = data?.tag as Tag

  if (!tag) {
    return <div>{t('loading')}</div>
  }

  return (
    <Fragment>
      <SEO title={tag.title} description={tag.description} />

      <h1>{tag.title}</h1>
      <Listing posts={tag?.posts} />
    </Fragment>
  )
}

export const getStaticProps: GetServerSideProps = async ({
  params,
  locale,
  defaultLocale,
}) => {
  if (typeof params?.slug !== 'string') {
    throw new Error('Slug should be a string')
  }

  const variables = {
    slug: params?.slug,
    locales: [
      ['es', 'it'].includes(locale as string) ? 'en' : locale,
      defaultLocale,
    ],
  }

  const { data, errors } = await query(PostsForTagDocument, variables)

  if (errors) {
    log.error('Failed to retrieve blog tag static page', { errors })

    throw new Error('Failed to retrieve blog tag page')
  }

  if (!data?.tag) {
    return { notFound: true }
  }

  return { props: { data }, revalidate: 10000 }
}

export const getStaticPaths: GetStaticPaths<ParsedUrlQuery> = async ({
  locales,
}) => {
  const paths: GetStaticPathsResult['paths'] = []

  const variables = {
    locales: ['en', 'fr'],
    includeCurrent: true,
  }

  const { data, errors } = await query(TagsSlugsDocument, variables)

  if (errors) {
    log.error('Failed to retrieve blog tag static paths', { errors })

    throw new Error('Failed to retrieve blog tag static paths')
  }

  data?.tags.forEach(({ slug }: { slug: string }) => {
    locales?.forEach((locale: string) => {
      paths.push({ params: { slug }, locale })
    })
  })

  return { paths, fallback: false }
}

export default BlogTagList

export { BlogTagList }
