import type { FC } from 'react'
import type { GetServerSideProps } from 'next'
import useTranslation from 'next-translate/useTranslation'

import { TagsListDocument, type TagsListQuery } from '@graphql/generated'
import SEO from '@components/SEO'
import { log } from '@utils/logger'
import { query } from '@graphql/query'
import { Content } from '@components/Layout'
import { TagGrid } from '@components/Tag'

const BlogTagList: FC<{ data: TagsListQuery }> = ({ data }) => {
  const { t } = useTranslation('blog')
  const tags = data?.tags

  return (
    <>
      <SEO title={t('tags.heading')} description={t('tags.description')} />

      <h1 className="mb-4">{t('tags.heading')}</h1>

      <Content>
        <TagGrid tags={tags} />
      </Content>
    </>
  )
}

export const getStaticProps: GetServerSideProps = async () => {
  const { data, errors } = await query(TagsListDocument)

  if (errors) {
    log.error('Failed to retrieve blog tag static page', { errors })

    throw new Error('Failed to retrieve blog tag page')
  }

  if (!data?.tags) {
    return { notFound: true }
  }

  return { props: { data }, revalidate: 10000 }
}

export default BlogTagList

export { BlogTagList }
