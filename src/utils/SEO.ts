import { mkdir, writeFile } from 'fs/promises'
import { globby } from 'globby'
import { Feed } from 'feed'

import Config from '../config'
import {
  type Page,
  type Post,
  type Rubrique,
  FeedDocument,
  SitemapDocument,
} from '@graphql/generated'
import { query } from '@graphql/query'
import { log } from '@utils/logger'
// import type { Category } from 'feed/lib/typings'

interface SitemapUrl {
  loc: string
  lastmod?: string
  frequency?: string
  priority?: number
}

class SitemapBuilder {
  #urlset: SitemapUrl[] = []

  addUrl(url: SitemapUrl) {
    this.#urlset.push(url)
  }

  #urlToXml(url: SitemapUrl) {
    return `
      <url>
        <loc>${url.loc}</loc>
        ${url?.lastmod ? `<lastmod>${url.lastmod}</lastmod>` : ''}
        ${url?.frequency ? `<frequency>${url.frequency}</frequency>` : ''}
        ${url?.priority ? `<priority>${url.priority}</priority>` : ''}
      </url>
    `
  }

  get() {
    return `<?xml version="1.0" encoding="UTF-8"?>
          <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ${this.#urlset.map(this.#urlToXml).join('')}
          </urlset>`
  }
}

const createSitemap = async () => {
  const sitemap = new SitemapBuilder()
  const { data, errors } = await query(SitemapDocument)

  if (errors?.length) {
    const errorMessage = 'Failed to retrieve sitemap urls'
    log.error(errorMessage, { errors })

    throw new Error(errorMessage)
  }

  const staticPages = await globby([
    'src/pages/**/*.tsx',
    '!src/pages/_*.tsx',
    '!src/pages/404.tsx',
    '!src/pages/sitemap.xml.tsx',
    '!src/pages/**/[slug].tsx',
  ])

  // const dynamicPages = await globby([
  //   'src/pages/**/[slug].tsx',
  // ])

  // console.log(dynamicPages)

  staticPages.forEach((page: string) => {
    const url = page
      .replace(/index$/, '')
      .replace(/src\/pages\/([^.]+).tsx/, '$1')

    sitemap.addUrl({ loc: `${Config.siteUrl}/${url}` })
  })

  data?.pages.forEach(
    ({ slug, publishedAt }: Pick<Page, 'slug' | 'publishedAt'>) => {
      sitemap.addUrl({ loc: `${Config.siteUrl}/${slug}`, lastmod: publishedAt })
    },
  )

  data?.rubriques.forEach(
    ({ slug, publishedAt }: Pick<Rubrique, 'slug' | 'publishedAt'>) => {
      sitemap.addUrl({
        loc: `${Config.siteUrl}/blog/category/${slug}`,
        lastmod: publishedAt,
      })
    },
  )

  data?.posts.forEach(
    ({ slug, publishedAt }: Pick<Post, 'slug' | 'publishedAt'>) => {
      sitemap.addUrl({
        loc: `${Config.siteUrl}/blog/${slug}`,
        lastmod: publishedAt,
      })
    },
  )

  return sitemap.get()
}

const generateSitemap = async () => {
  const sitemap = await createSitemap()

  await writeFile('./public/sitemap.xml', sitemap)
}

const createFeed = async () => {
  const { data, errors } = await query(FeedDocument)

  if (errors?.length) {
    const errorMessage = 'Failed to retrieve feed data'
    log.error(errorMessage, { errors })

    throw new Error(errorMessage)
  }

  const feed = new Feed({
    title: "Ousmane Ndiaye's Blog",
    description: 'This is my personal feed!',
    id: Config.siteUrl,
    link: Config.siteUrl,
    language: 'en', // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
    image: 'http://example.com/image.png',
    favicon: `${Config.siteUrl}/favicon.ico`,
    copyright: 'All rights reserved 2023, Ousmane Ndiaye',
    updated: new Date(2013, 6, 14), // optional, default = today
    generator: 'awesome', // optional, default = 'Feed for Node.js'
    feedLinks: {
      json: `${Config.siteUrl}/rss/feed.json`,
      atom: `${Config.siteUrl}/rss/atom.xml`,
      // rss ?
    },
    author: {
      name: 'Ousmane Ndiaye Ndiaye',
      link: 'https://ornous.vercel.app',
    },
  })

  data?.posts.forEach((post: Post) => {
    const postUrl = `${Config.siteUrl}/blog/${post.slug}`

    feed.addItem({
      title: post.title,
      id: postUrl,
      link: postUrl,
      description: post.description,
      content: post.content,
      // category: [{ name: post?.rubrique?.name }],
      date: new Date(post.createdAt),
      image: post.heroImage?.url,
    })
  })

  // data?.rubriques.forEach((rubrique: Rubrique) => {
  //   feed.addCategory(rubrique.name)
  // })

  return feed
}

const generateFeedFiles = async () => {
  const feed = await createFeed()

  await mkdir('./public/rss', { recursive: true })
  await writeFile('./public/rss/feed.xml', feed.rss2())
  await writeFile('./public/rss/atom.xml', feed.atom1())
  await writeFile('./public/rss/feed.json', feed.json1())
}

export { createSitemap, createFeed, generateSitemap, generateFeedFiles }
