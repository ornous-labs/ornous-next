import { formatDate } from '../i18n'

describe('@utils/i18n', () => {
  const sampleDate = '2022-06-03T13:07:23.521984+00:00'

  describe('formatDate', () => {
    it('formats a date string to English by default', () => {
      expect(formatDate(sampleDate)).toStrictEqual('3 June 2022')
    })

    it('formats a date string to a specific locale', () => {
      expect(formatDate(sampleDate, 'en')).toStrictEqual('3 June 2022')
      expect(formatDate(sampleDate, 'fr')).toStrictEqual('3 juin 2022')
    })

    it('formats a date string to a specific format', () => {
      const options: Intl.DateTimeFormatOptions = {
        weekday: 'long',
        day: 'numeric',
        month: 'long',
        year: 'numeric',
      }

      expect(formatDate(sampleDate, 'en', options)).toStrictEqual(
        'Friday, 3 June 2022',
      )
      expect(formatDate(sampleDate, 'fr', options)).toStrictEqual(
        'vendredi 3 juin 2022',
      )
    })

    it('throws when passed an invalid date value', () => {
      expect(() => formatDate('18/01/1987')).toThrow('Invalid date value')
    })
  })
})
