// import { createElement, Fragment } from 'react'
import { unified } from 'unified'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import remarkGfm from 'remark-gfm'
import remarkEmoji from 'remark-emoji'
import remarkSmartypants from 'remark-smartypants'
// import rehypeReact from 'rehype-react'
import rehypeHighlight from 'rehype-highlight'
import rehypeSlug from 'rehype-slug'
import rehypeAutolinkHeadings from 'rehype-autolink-headings'
import rehypeToc from 'rehype-toc'
import rehypeShiftHeading from 'rehype-shift-heading'
import rehypeStringify from 'rehype-stringify'
import rehypeExtractExcerpt from 'rehype-extract-excerpt'
import { h, s } from 'hastscript'
import { toString } from 'hast-util-to-string'

/*
 * TODO unified rehype checklist
 * Phase 1
 * - [x] Syntax Highlighting
 * - [x] header links
 * - [x] toc
 *
 * Phase 2
 * - [x] read time
 * - [x] emoji short codes
 * - [x] Smarty Pants
 * - [x] excerpt extraction
 * - [ ] next image component to replace img
 *
 * Phase 3
 * - [>] Mermaid diagrams
 * - [ ] next link component to replace local a
 * - [ ] Video embed
 * - [ ] Tweet Embed
 * - [ ] copy header link on icon click
 *
 * Considerations
 * - moderation
 * - Sanitization
 */

export const processMarkdown = (markdown: string) => {
  // const components = {
  //   // a: Link,
  //   img: (...props: any[]) => Image,
  // }

  const pipeline = unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(remarkEmoji)
    .use(remarkSmartypants)
    .use(remarkRehype)
    .use(rehypeSlug)
    .use(rehypeToc)
    .use(rehypeAutolinkHeadings, {
      behavior: 'prepend',
      content(node) {
        return [
          // TODO translate visuially hidden text
          h('span.sr-only', 'Read the “', toString(node), '” section'),
          s(
            'svg',
            {
              xmlns: 'http://www.w3.org/2000/svg',
              viewBox: '0 0 640 512',
            },
            [
              s('path', {
                d: 'M579.8 267.7c56.5-56.5 56.5-148 0-204.5c-50-50-128.8-56.5-186.3-15.4l-1.6 1.1c-14.4 10.3-17.7 30.3-7.4 44.6s30.3 17.7 44.6 7.4l1.6-1.1c32.1-22.9 76-19.3 103.8 8.6c31.5 31.5 31.5 82.5 0 114L422.3 334.8c-31.5 31.5-82.5 31.5-114 0c-27.9-27.9-31.5-71.8-8.6-103.8l1.1-1.6c10.3-14.4 6.9-34.4-7.4-44.6s-34.4-6.9-44.6 7.4l-1.1 1.6C206.5 251.2 213 330 263 380c56.5 56.5 148 56.5 204.5 0L579.8 267.7zM60.2 244.3c-56.5 56.5-56.5 148 0 204.5c50 50 128.8 56.5 186.3 15.4l1.6-1.1c14.4-10.3 17.7-30.3 7.4-44.6s-30.3-17.7-44.6-7.4l-1.6 1.1c-32.1 22.9-76 19.3-103.8-8.6C74 372 74 321 105.5 289.5L217.7 177.2c31.5-31.5 82.5-31.5 114 0c27.9 27.9 31.5 71.8 8.6 103.9l-1.1 1.6c-10.3 14.4-6.9 34.4 7.4 44.6s34.4 6.9 44.6-7.4l1.1-1.6C433.5 260.8 427 182 377 132c-56.5-56.5-148-56.5-204.5 0L60.2 244.3z',
              }),
            ],
          ),
        ]
      },
    })
    .use(rehypeHighlight, { ignoreMissing: true })
    .use(rehypeShiftHeading, { shift: -1 })
    .use(rehypeStringify)

  return String(pipeline.processSync(markdown))
}

const extractMarkdownToc = async (markdown: string) => {
  const rehypeExtractToc = (await import('@stefanprobst/rehype-extract-toc'))
    .default

  const pipeline = unified()
    .use(remarkParse)
    .use(remarkRehype)
    .use(rehypeSlug)
    .use(rehypeExtractToc)
    .use(rehypeStringify)

  const file = await pipeline.process(markdown)

  return file.data.toc
}

const extractMarkdownExcerpt = async (markdown: string, maxLength?: number) => {
  // const rehypeExtractExcerpt = (await import('rehype-extract-excerpt')).default

  const pipeline = unified()
    .use(remarkParse)
    .use(remarkRehype)
    .use(rehypeSlug)
    .use(rehypeExtractExcerpt, { maxLength })
    .use(rehypeStringify)

  const file = await pipeline.process(markdown)

  return file.data.excerpt ?? ''
}

export { extractMarkdownExcerpt, extractMarkdownToc }
