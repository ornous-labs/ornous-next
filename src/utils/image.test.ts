import { graphcmsImageLoader } from './image'

describe('graphcms image loader', () => {
  it('valid url', () => {
    const result = graphcmsImageLoader({
      src: 'https://media.graphassets.com/test.jpg',
      width: 340,
    })
    expect(result).toEqual(
      'https://media.graphassets.com/resize=fit:clip,width:340/test.jpg',
    )
  })
  it('throws error for local urls', () => {
    expect(() => {
      graphcmsImageLoader({ src: 'test.jpg', width: 340 })
    }).toThrow('Not a valid graphcms image url')
  })

  it('throws error for non https', () => {
    expect(() => {
      graphcmsImageLoader({
        src: 'http://media.graphassets.com/test.jpg',
        width: 340,
      })
    }).toThrow('https image url is required. received http:')
  })

  it('throws error for remote non graphcms urls', () => {
    expect(() => {
      graphcmsImageLoader({ src: 'https://unsafe.domain/test.jpg', width: 340 })
    }).toThrow('Not a valid graphcms image url')
  })

  // TODO work out more appropriate logic. One that supports aspect ratio
  it('Replaces already existing values if set(temporary)', () => {
    const src =
      'https://media.graphassets.com/resize=fit:clip,width:800/lG9ujlSATeWQQ4i2Hj8I'
    // const src = 'https://media.graphassets.com/resize=width:340/test.jpg'
    const result = graphcmsImageLoader({ src, width: 320 })

    expect(result).toEqual(src.replace('800', '320'))
    expect(result).toEqual(expect.stringContaining('width:320'))
  })
})
