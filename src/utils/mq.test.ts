import mq from './mq'

describe('utils.mq', () => {
  it('valid url', () => {
    const result = mq({ fontSize: ['100%', '112.5%', '120%', '130%'] })
    expect(result).toMatchInlineSnapshot(`
      [
        {
          "@media (min-width: 576px)": {
            "fontSize": "112.5%",
          },
          "@media (min-width: 768px)": {
            "fontSize": "120%",
          },
          "@media (min-width: 992px)": {
            "fontSize": "130%",
          },
          "fontSize": "100%",
        },
      ]
    `)
  })
})
