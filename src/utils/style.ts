export const renderStatic = async (html: string) => {
  const createServer = (await import('@emotion/server/create-instance')).default
  const { cache } = await import('@emotion/css')

  if (html === undefined) {
    throw new Error('did you forget to return html from renderToString?')
  }
  const { extractCritical } = createServer(cache)
  const { ids, css } = extractCritical(html)

  return { html, ids, css }
}
