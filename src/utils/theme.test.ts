import { addShadesToTheme } from './theme'

describe('utils.theme', () => {
  describe('addShadesToTheme', () => {
    it('generates color shades for base colors', () => {
      const theme = {
        colors: {
          primary: 'rgb(255, 0, 0)',
        },
      }
      const { colors } = addShadesToTheme(theme)

      expect(colors.primary.default).toEqual('255 0 0')
      expect(colors.primary['500']).toEqual('255 0 0')
      expect(Object.keys(colors.primary)).toHaveLength(11)
    })
  })

  it('does not generate color shades if theme defines shades', () => {
    const theme = {
      colors: {
        primary: {
          '50': 'rgb(255, 57, 57)',
          '100': 'red',
          default: 'rgb(255, 0, 0)',
        },
      },
    }

    const { colors } = addShadesToTheme(theme)

    expect(colors.primary).toEqual({
      default: '255 0 0',
      '50': '255 57 57',
      '100': '255 0 0',
    })
  })
})
