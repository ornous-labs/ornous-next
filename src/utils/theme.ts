import type { Theme, ThemeMode } from '@emotion/react'
import { darken, desaturate, modularScale, parseToRgb } from 'polished'
import { log } from '../utils/logger'

const shadesNames: number[] = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900]

const toRgb = (color: string) => {
  return Object.values(parseToRgb(color)).join(' ')
}

const camelCaseToSnakeCase = (str: string) => {
  return str.replace(/[A-Z]/g, (char) => `-${char.toLowerCase()}`)
}

const generateColorShades =
  (mode: ThemeMode) =>
  ([name, baseColor]: [string, string | object]) => {
    if (typeof baseColor === 'object') {
      const shades = Object.entries(baseColor).map(([name, baseColor]) => [
        name,
        toRgb(baseColor),
      ])

      return [name, Object.fromEntries(shades)]
    }

    // Means we lighten in light mode and darken in dark mode
    const dir = mode === 'light' ? 1 : -1
    const coeffLuminosity = 0.024
    const coeffSaturation = 0.012 * -1
    const shades = shadesNames.map((shadeName) => {
      // const intensity = Math.floor(shadeNames.length / 2) - shadeName / 100
      const intensity = dir * (5 - shadeName / 100)

      const lumValue = coeffLuminosity * intensity
      const satValue = coeffSaturation * intensity
      const shade = darken(lumValue, desaturate(satValue, baseColor))

      return [shadeName, toRgb(shade)]
    })

    return [
      name,
      {
        default: toRgb(baseColor),
        ...Object.fromEntries(shades),
      },
    ]
  }

export const addShadesToTheme = ({ colors, ...theme }: Theme) => {
  // TODO move away from hackiness
  const shadedColors = Object.fromEntries(
    Object.entries(colors).map(generateColorShades(theme.mode)),
  )

  return { ...theme, colors: shadedColors }
}

export const themeToCustomProps = (rawTheme: Theme) => {
  const theme = addShadesToTheme(rawTheme)

  const colorProps = Object.entries(theme.colors).flatMap(([key, color]) => {
    const colorName = camelCaseToSnakeCase(key)
    if (typeof color === 'string') {
      return `--color-${colorName}: ${toRgb(color)};`
    }

    const shades = Object.entries(color).map(([name, shade]) => {
      const shadeName = name === 'default' ? colorName : `${colorName}-${name}`
      return `--color-${shadeName}: ${shade};`
    })

    return shades
  })

  const ratio = theme?.spacing?.ratio
  const { base: textBase } = theme.fonts.text
  const { base: headingBase } = theme.fonts.heading

  // TODO clean up temporary work
  let modeOverrides
  if (theme.mode === 'dark') {
    modeOverrides = `
      img {
        filter: brightness(0.8) contrast(1.2);
        transition: filter var(--transition-med) ease-in-out;

        &:hover {
          filter: brightness(1) contrast(1);
        }
      }
    `
  }

  const props = `
    ${colorProps.join('\n')}

    --font-heading: ${theme.fonts.heading.family};
    --font-text: ${theme.fonts.text.family};

    --font-text-2xs: ${modularScale(-3, textBase, ratio)};
    --font-text-xs: ${modularScale(-2, textBase, ratio)};
    --font-text-s: ${modularScale(-1, textBase, ratio)};
    --font-text-m: ${modularScale(0, textBase, ratio)};
    --font-text-l: ${modularScale(1, textBase, ratio)};

    --font-heading-xs: ${modularScale(-2, headingBase, ratio)};
    --font-heading-s: ${modularScale(-1, headingBase, ratio)};
    --font-heading-m: ${modularScale(0, headingBase, ratio)};
    --font-heading-l: ${modularScale(1, headingBase, ratio)};
    --font-heading-xl: ${modularScale(2, headingBase, ratio)};
    --font-heading-2xl: ${modularScale(3, headingBase, ratio)};
    --font-heading-3xl: ${modularScale(4, headingBase, ratio)};

    --transition-xslow: 1s;
    --transition-slow: 0.8s;
    --transition-med: 0.3s;
    --transition-fast: 0.2s;
    --transition-xfast: 0.1s;

    --shadow-outline: 0 0 0 3px rgba(0, 0, 0, 0.5);
    --shadow-xs: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    --shadow-s: 0 3px 5px rgba(0, 0, 0, 0.15), 0 2px 4px rgba(0, 0, 0, 0.12);
    --shadow-m: 0 10px 20px rgba(0, 0, 0, 0.15), 0 3px 6px rgba(0, 0, 0, 0.10);
    --shadow-l: 0 15px 25px rgba(0, 0, 0, 0.15), 0 5px 10px rgba(0, 0, 0, 0.05);
    --shadow-xl: 0 20px 40px rgba(0, 0, 0, 0.20);


    ${modeOverrides}
  `

  return props
}
