const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.tsx',
    './src/components/**/*.tsx',
    './src/layouts/**/*.tsx',
  ],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        // black: colors.black,
        // white: colors.white,
        // TODO Make this selectable from theme
        gray: colors.slate,

        background: {
          DEFAULT: 'rgb(var(--color-background) / <alpha-value>)',
          50: 'rgb(var(--color-background-50) / <alpha-value>)',
          100: 'rgb(var(--color-background-100) / <alpha-value>)',
          200: 'rgb(var(--color-background-200) / <alpha-value>)',
          300: 'rgb(var(--color-background-300) / <alpha-value>)',
          400: 'rgb(var(--color-background-400) / <alpha-value>)',
          500: 'rgb(var(--color-background-500) / <alpha-value>)',
          600: 'rgb(var(--color-background-600) / <alpha-value>)',
          700: 'rgb(var(--color-background-700) / <alpha-value>)',
          800: 'rgb(var(--color-background-800) / <alpha-value>)',
          900: 'rgb(var(--color-background-900) / <alpha-value>)',
        },

        text: {
          DEFAULT: 'rgb(var(--color-text) / <alpha-value>)',
          50: 'rgb(var(--color-text-50) / <alpha-value>)',
          100: 'rgb(var(--color-text-100) / <alpha-value>)',
          200: 'rgb(var(--color-text-200) / <alpha-value>)',
          300: 'rgb(var(--color-text-300) / <alpha-value>)',
          400: 'rgb(var(--color-text-400) / <alpha-value>)',
          500: 'rgb(var(--color-text-500) / <alpha-value>)',
          600: 'rgb(var(--color-text-600) / <alpha-value>)',
          700: 'rgb(var(--color-text-700) / <alpha-value>)',
          800: 'rgb(var(--color-text-800) / <alpha-value>)',
          900: 'rgb(var(--color-text-900) / <alpha-value>)',
        },

        'on-background': 'rgba(var(--color-on-background) / <alpha-value>)',

        primary: {
          DEFAULT: 'rgb(var(--color-primary) / <alpha-value>)',
          50: 'rgb(var(--color-primary-50) / <alpha-value>)',
          100: 'rgb(var(--color-primary-100) / <alpha-value>)',
          200: 'rgb(var(--color-primary-200) / <alpha-value>)',
          300: 'rgb(var(--color-primary-300) / <alpha-value>)',
          400: 'rgb(var(--color-primary-400) / <alpha-value>)',
          500: 'rgb(var(--color-primary-500) / <alpha-value>)',
          600: 'rgb(var(--color-primary-600) / <alpha-value>)',
          700: 'rgb(var(--color-primary-700) / <alpha-value>)',
          800: 'rgb(var(--color-primary-800) / <alpha-value>)',
          900: 'rgb(var(--color-primary-900) / <alpha-value>)',
        },
        'on-primary': 'rgb(var(--color-on-primary) / <alpha-value>)',

        secondary: {
          DEFAULT: 'rgb(var(--color-secondary) / <alpha-value>)',
          50: 'rgb(var(--color-secondary-50) / <alpha-value>)',
          100: 'rgb(var(--color-secondary-100) / <alpha-value>)',
          200: 'rgb(var(--color-secondary-200) / <alpha-value>)',
          300: 'rgb(var(--color-secondary-300) / <alpha-value>)',
          400: 'rgb(var(--color-secondary-400) / <alpha-value>)',
          500: 'rgb(var(--color-secondary-500) / <alpha-value>)',
          600: 'rgb(var(--color-secondary-600) / <alpha-value>)',
          700: 'rgb(var(--color-secondary-700) / <alpha-value>)',
          800: 'rgb(var(--color-secondary-800) / <alpha-value>)',
          900: 'rgb(var(--color-secondary-900) / <alpha-value>)',
        },
        'on-secondary': 'rgb(var(--color-on-secondary) / <alpha-value>)',

        accent: {
          DEFAULT: 'rgb(var(--color-accent) / <alpha-value>)',
          50: 'rgb(var(--color-accent-50) / <alpha-value>)',
          100: 'rgb(var(--color-accent-100) / <alpha-value>)',
          200: 'rgb(var(--color-accent-200) / <alpha-value>)',
          300: 'rgb(var(--color-accent-300) / <alpha-value>)',
          400: 'rgb(var(--color-accent-400) / <alpha-value>)',
          500: 'rgb(var(--color-accent-500) / <alpha-value>)',
          600: 'rgb(var(--color-accent-600) / <alpha-value>)',
          700: 'rgb(var(--color-accent-700) / <alpha-value>)',
          800: 'rgb(var(--color-accent-800) / <alpha-value>)',
          900: 'rgb(var(--color-accent-900) / <alpha-value>)',
        },

        // denotive colors
        error: 'rgba(var(--color-error) / <alpha-value>)',
        'on-error': 'rgba(var(--color-on-error) / <alpha-value>)',
      },
      fontSize: {
        sm: 'var(--font-text-s)',
        base: 'var(--font-text-m)',
        l: 'var(--font-heading-l)',
        xl: 'var(--font-heading-xl)',
        '2xl': 'var(--font-heading-2xl)',
        '3xl': 'var(--font-heading-3xl)',
      },
      fontFamily: {
        sans: 'Lato',
        text: 'var(--font-text)',
        heading: 'var(--font-heading)',
      },
      typography: () => ({
        DEFAULT: {
          css: {
            a: {
              '&:where(:hover, :focus-visible)': {
                color: 'rgb(var(--color-primary-900))',
                outline: 'none',
              },
            },
            pre: {
              'box-shadow': '2px 2px 4px 0 rgb(var(--color-shadow) / 20%)',
            },
            '--tw-prose-body': 'rgb(var(--color-text-200))',
            '--tw-prose-headings': 'rgb(var(--color-text-200))',
            '--tw-prose-quotes': 'rgb(var(--color-text))',
            '--tw-prose-lead': 'rgb(var(--color-text-500))',
            // TODO Replace with a shade of grey
            '--tw-prose-pre-code': 'rgb(var(--color-text-500))',
            '--tw-prose-pre-bg': 'rgb(var(--color-code-highlight-background))',
            '--tw-prose-links': 'rgb(var(--color-primary-50))',
          },
        },
      }),
    },
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')],
}
