export default {
  src: '/test-file-stub.png',
  width: 24,
  height: 24,
  blurDataURL: 'data:image/png;base64,imagedata',
}
