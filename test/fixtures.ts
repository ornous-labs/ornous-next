const image = {
  url: 'http://localhost:3000/img',
  width: 340,
  height: 340,
  dataURL: 'base64img',
}

const rubrique: unknown = {
  id: '1',
  slug: 'category-name',
  name: 'Category Name',
  colour: {
    hex: '#000000',
  },
  relatedPosts: [],
  localizations: [
    {
      id: '1',
      slug: 'nom-de-la-categorie',
      name: 'Nom de la Categorie',
      locale: 'fr',
    },
  ],
}

const post: unknown = {
  id: '1',
  slug: 'post-title',
  locale: 'en',
  title: 'Post Title',
  publishedAt: '1970-01-01',
  description:
    'hello world is a greeting used to show things work on a basic level',
  heroImage: image,
  md: '# title 1',
  readingTime: {
    minutes: 5.76,
  },
  content: '<h1>title 1</h1>',
  tags: [],
  rubrique,
  localizations: [
    {
      id: '1',
      slug: 'titre-de-l-article',
      title: "Titre de l'article",
      locale: 'fr',
      rubrique,
    },
  ],
}

rubrique.posts = [post]

export { image, post, rubrique }
