import '@testing-library/jest-dom'
import { TextEncoder } from 'util'
import _ from 'lodash'
// import { jest } from '@jest/globals'

// jest.unstable_mockModule('next/router', () => import('next-router-mock'))
jest.mock('next/router', () => require('next-router-mock'))

import Router from 'next/router'
Router.locale = 'en'
Router.locales = ['en', 'fr']

global.TextEncoder = TextEncoder

// jest.unstable_mockModule('next-translate/useTranslation', () => {
jest.mock('next-translate/useTranslation', () => {
  return () => ({
    t: (str: string, ctx?: Record<string, string>) =>
      ctx ? `${str} ${JSON.stringify(ctx)}` : str,
  })
})

// Not great but prevents errors caused by jsdom not supporting @container
const originalConsoleError = console.error
console.error = function (msg) {
  if (_.startsWith(msg, 'Error: Could not parse CSS stylesheet')) return

  originalConsoleError(msg)
}

process.env = {
  ...process.env,
  // @ts-expect-error will expect a string
  __NEXT_IMAGE_OPTS: {
    experimentalFuture: true,
    deviceSizes: [320, 420, 768, 1024, 1200],
    imageSizes: [20, 40, 60, 70, 200, 400, 440],
    domains: ['domain.net'],
    path: '/_next/future/image/',
    loader: 'default',
  },
}
