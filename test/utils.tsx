import { render, type RenderOptions } from '@testing-library/react'
import type { FC, ReactNode } from 'react'
import { ThemeProvider } from '@emotion/react'
import { mockAnimationsApi } from 'jsdom-testing-mocks'

import theme from '../src/theme/themes/warm-antique'
import { createTheme } from '../src/theme/utils'
import '../src/icons-library'

mockAnimationsApi()

const AppProvider: FC<{ children: ReactNode }> = ({ children }) => {
  return <ThemeProvider theme={createTheme(theme)}>{children}</ThemeProvider>
}
const wrappedRender = (
  ui: JSX.Element,
  options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, { wrapper: AppProvider, ...options })

export { wrappedRender as render }
export { default as userEvent } from '@testing-library/user-event'
export {
  act,
  cleanup,
  fireEvent,
  renderHook,
  screen,
  waitForElementToBeRemoved,
  within,
} from '@testing-library/react'
export { mockViewport } from 'jsdom-testing-mocks'
